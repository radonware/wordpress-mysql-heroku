<?php
// Store user defined options
$wpc2 = array();
// Store default options;
$wpc2_default = array();

// Grid Section
$wpc2_default['site_width'] = '1100';
$wpc2_default['sidebar_width'] = '272';
$wpc2_default['edge_padding'] = '30';
$wpc2_default['sidebar_edge_padding'] = '60';

// Body Background
$wpc2_default['body_background_image'] = '';
$wpc2_default['body_background_position'] = 'center top';
$wpc2_default['body_background_repeat'] = 'repeat-x';
$wpc2_default['body_background_attachment'] = 'scroll';
$wpc2_default['body_background_color'] = '#ffffff';

// Body Font
$wpc2_default['body_font_family'] = 'Lato';
$wpc2_default['body_font_smoothing'] = '';
$wpc2_default['body_font_size'] = '17';
$wpc2_default['body_font_text_transform'] = 'none';
$wpc2_default['body_font_weight'] = '300';
$wpc2_default['body_font_bold_weight'] = 'bold';
$wpc2_default['body_font_style'] = 'normal';
$wpc2_default['body_font_line_height'] = '1.6';
$wpc2_default['body_font_color'] = '#5a5958';
$wpc2_default['body_link_font_color'] = '#fb486e';
$wpc2_default['body_link_font_hover_color'] = '#ff4da6';
$wpc2_default['body_font_size_xsmall'] = '13';
$wpc2_default['body_font_size_small'] = '15';
$wpc2_default['body_font_size_large'] = '19';
$wpc2_default['body_font_size_xlarge'] = '21';
$wpc2_default['code_font_size'] = '15';

// Logo Image
$wpc2_default['logo_image'] = '';
$wpc2_default['logo_bottom_padding'] = '20';

// Site Title & Tagline
$wpc2_default['show_title'] = '1';
$wpc2_default['show_description'] = '1';
$wpc2_default['title_bottom_padding'] = '10';

// Title Font
$wpc2_default['title_font_family'] = 'Playfair Display';
$wpc2_default['title_font_smoothing'] = '1';
$wpc2_default['title_font_size'] = '60';
$wpc2_default['title_font_text_transform'] = 'lowercase';
$wpc2_default['title_font_weight'] = 'normal';
$wpc2_default['title_font_style'] = 'normal';
$wpc2_default['title_font_line_height'] = '1.0';
$wpc2_default['title_font_color'] = '#a0a0a0';

// Description Font
$wpc2_default['description_font_family'] = 'Lato';
$wpc2_default['description_font_smoothing'] = '1';
$wpc2_default['description_font_size'] = '17';
$wpc2_default['description_font_text_transform'] = 'capitalize';
$wpc2_default['description_font_weight'] = '300';
$wpc2_default['description_font_style'] = 'normal';
$wpc2_default['description_font_line_height'] = '1.3';
$wpc2_default['description_font_color'] = '#adadad';

// Header Layout
$wpc2_default['header_background_image'] = get_template_directory_uri() . '/img/header-background.png';
$wpc2_default['header_background_position'] = 'center top';
$wpc2_default['header_background_repeat'] = 'repeat-x';
$wpc2_default['header_background_color'] = '';
$wpc2_default['header_top_padding'] = '80';
$wpc2_default['header_bottom_padding'] = '20';
$wpc2_default['header_show_social_icons'] = '';

// Menu Font
$wpc2_default['menu_font_family'] = 'Lato';
$wpc2_default['menu_font_smoothing'] = '1';
$wpc2_default['menu_font_size'] = '17';
$wpc2_default['menu_font_text_transform'] = 'lowercase';
$wpc2_default['menu_font_weight'] = '300';
$wpc2_default['menu_font_style'] = 'normal';
$wpc2_default['menu_font_line_height'] = '1.0';

// Menu Bar
$wpc2_default['menu_bar_background_color'] = '#ffffff';
$wpc2_default['menu_bar_border_color'] = '#dddddd';
$wpc2_default['menu_bar_font_color'] = '#a0a0a0';
$wpc2_default['menu_bar_font_hover_color'] = '#ff4da6';
$wpc2_default['menu_bar_item_side_padding'] = '12';
$wpc2_default['menu_bar_item_top_padding'] = '12';
$wpc2_default['menu_bar_item_bottom_padding'] = '12';
$wpc2_default['menu_bar_enable_sticky_menu'] = '1';

// Dropdown Menu
$wpc2_default['dropdown_background_color'] = '#f3f3f3';
$wpc2_default['dropdown_font_color'] = '#a8a8a8';
$wpc2_default['dropdown_font_hover_color'] = '#ff4da6';
$wpc2_default['dropdown_item_top_padding'] = '10';
$wpc2_default['dropdown_item_bottom_padding'] = '10';

// Page Title Font
$wpc2_default['page_title_font_family'] = 'Lato';
$wpc2_default['page_title_font_smoothing'] = '1';
$wpc2_default['page_title_font_size'] = '40';
$wpc2_default['page_title_font_text_transform'] = 'none';
$wpc2_default['page_title_font_weight'] = '300';
$wpc2_default['page_title_font_bold_weight'] = 'normal';
$wpc2_default['page_title_font_style'] = 'normal';
$wpc2_default['page_title_font_line_height'] = '1.3';
$wpc2_default['page_title_font_color'] = '#cccccc';

// Heading Font
$wpc2_default['heading_font_family'] = 'Lato';
$wpc2_default['heading_font_smoothing'] = '1';
$wpc2_default['heading_font_text_transform'] = 'none';
$wpc2_default['heading_font_weight'] = '300';
$wpc2_default['heading_font_bold_weight'] = 'normal';
$wpc2_default['heading_font_style'] = 'normal';
$wpc2_default['heading_font_line_height'] = '1.2';
$wpc2_default['heading_font_color'] = '#a9a9a9';
$wpc2_default['heading_link_font_color'] = '#ff4da6';
$wpc2_default['heading_link_font_hover_color'] = '#fb486e';

// Heading Font Sizes
$wpc2_default['heading_font_size_h1'] = '32';
$wpc2_default['heading_font_size_h2'] = '28';
$wpc2_default['heading_font_size_h3'] = '24';
$wpc2_default['heading_font_size_h4'] = '18';
$wpc2_default['heading_font_size_h5'] = '16';
$wpc2_default['heading_font_size_h6'] = '15';
$wpc2_default['heading_h4_font_text_transform'] = 'uppercase';
$wpc2_default['heading_h4_font_weight'] = 'normal';
$wpc2_default['heading_h4_font_style'] = 'normal';
$wpc2_default['heading_h4_font_color'] = '#cccccc';

// Date Font
$wpc2_default['date_font_family'] = 'Lato';
$wpc2_default['date_font_smoothing'] = '1';
$wpc2_default['date_font_size'] = '20';
$wpc2_default['date_font_text_transform'] = 'none';
$wpc2_default['date_font_weight'] = '300';
$wpc2_default['date_font_style'] = 'normal';
$wpc2_default['date_font_line_height'] = '1.2';
$wpc2_default['date_font_color'] = '#a3abaf';

// Quote Font
$wpc2_default['quote_font_family'] = 'Lato';
$wpc2_default['quote_font_smoothing'] = '1';
$wpc2_default['quote_font_size'] = '22';
$wpc2_default['quote_font_text_transform'] = 'none';
$wpc2_default['quote_font_weight'] = 'normal';
$wpc2_default['quote_font_style'] = 'italic';
$wpc2_default['quote_font_line_height'] = '1.5';

// Post Meta Font
$wpc2_default['post_meta_font_family'] = 'Lato';
$wpc2_default['post_meta_font_smoothing'] = '1';
$wpc2_default['post_meta_font_size'] = '16';
$wpc2_default['post_meta_font_text_transform'] = 'lowercase';
$wpc2_default['post_meta_font_weight'] = '300';
$wpc2_default['post_meta_font_style'] = 'normal';
$wpc2_default['post_meta_font_color'] = '#a9a9a9';
$wpc2_default['post_meta_font_hover_color'] = '#fb486e';
$wpc2_default['archive_meta_font_color'] = '#c9c9c9';
$wpc2_default['archive_meta_font_hover_color'] = '#626262';

// Buttons 
$wpc2_default['button_font_family'] = 'Lato';
$wpc2_default['button_font_smoothing'] = '1';
$wpc2_default['button_font_size'] = '17';
$wpc2_default['button_font_text_transform'] = 'none';
$wpc2_default['button_font_weight'] = 'normal';
$wpc2_default['button_font_style'] = 'normal';
$wpc2_default['button_font_color'] = '#cccccc';
$wpc2_default['button_font_background_color'] = '#f3f3f3';
$wpc2_default['button_top_padding'] = '10';
$wpc2_default['button_bottom_padding'] = '10';
$wpc2_default['button_side_padding'] = '15';
$wpc2_default['button_icon_offset'] = '0';

// Content
$wpc2_default['content_background_color'] = '#ffffff';
$wpc2_default['content_accent_color'] = '#f5f5f5';
$wpc2_default['border_color'] = '#f3f3f3';
$wpc2_default['content_top_padding'] = '60';
$wpc2_default['form_font_color'] = '#b1b0af';
$wpc2_default['form_border_color'] = '#f3f3f3';
$wpc2_default['form_border_focus_color'] = '#fb486e';
$wpc2_default['caption_font_color'] = '#777777';
$wpc2_default['caption_link_font_color'] = '#fb486e';
$wpc2_default['caption_font_background_color'] = '#f3f3f3';
$wpc2_default['caption_font_background_opacity'] = '0.9';
$wpc2_default['comment_background_color'] = '#f5f5f5';

// Blog
$wpc2_default['excerpt_show_on_blog'] = '';
$wpc2_default['excerpt_show_on_archive'] = '';
$wpc2_default['excerpt_show_on_search'] = '1';
$wpc2_default['archive_meta_show'] = '1';
$wpc2_default['post_nav_show'] = '1';

// Sidebar Dislpay
$wpc2_default['sidebar_position'] = 'right';

// Widget Title Font
$wpc2_default['sidebar_widget_title_font_family'] = 'Lato';
$wpc2_default['sidebar_widget_title_font_smoothing'] = '';
$wpc2_default['sidebar_widget_title_font_size'] = '17';
$wpc2_default['sidebar_widget_title_font_text_transform'] = 'none';
$wpc2_default['sidebar_widget_title_font_weight'] = '300';
$wpc2_default['sidebar_widget_title_font_style'] = 'normal';
$wpc2_default['sidebar_widget_title_font_line_height'] = '2.0';

// Sidebar Widgets
$wpc2_default['sidebar_padding'] = '20';
$wpc2_default['sidebar_background_color'] = '#f3f3f3';
$wpc2_default['sidebar_accent_color'] = '#ffffff';
$wpc2_default['sidebar_title_font_color'] = '#a0a0a0';
$wpc2_default['sidebar_title_font_background_color'] = '#ffffff';
$wpc2_default['sidebar_font_color'] = '#888888';
$wpc2_default['sidebar_link_font_color'] = '#cccccc';
$wpc2_default['sidebar_link_font_hover_color'] = '#ff4da6';
$wpc2_default['sidebar_border_color'] = '#cccccc';
$wpc2_default['sidebar_button_font_color'] = '#ff4da6';
$wpc2_default['sidebar_button_font_background_color'] = '#ededed';
$wpc2_default['sidebar_form_border_color'] = '#ededed';
$wpc2_default['sidebar_form_border_focus_color'] = '#aaaaaa';

// Footer Widget Title Font
$wpc2_default['footer_widget_title_font_family'] = 'Lato';
$wpc2_default['footer_widget_title_font_smoothing'] = '';
$wpc2_default['footer_widget_title_font_size'] = '17';
$wpc2_default['footer_widget_title_font_text_transform'] = 'none';
$wpc2_default['footer_widget_title_font_weight'] = '300';
$wpc2_default['footer_widget_title_font_style'] = 'normal';
$wpc2_default['footer_widget_title_font_line_height'] = '2.0';

// Footer Widgets
$wpc2_default['footer_widget_width'] = '320';
$wpc2_default['footer_gutter'] = '40';
$wpc2_default['footer_background_color'] = '#f3f3f3';
$wpc2_default['footer_accent_color'] = '#dbdbda';
$wpc2_default['footer_title_font_color'] = '#b1b0af';
$wpc2_default['footer_title_font_background_color'] = '#ffffff';
$wpc2_default['footer_font_color'] = '#b1b0af';
$wpc2_default['footer_link_font_color'] = '#ff4da6';
$wpc2_default['footer_link_font_hover_color'] = '#fb486e';
$wpc2_default['footer_border_color'] = '#f3f3f3';
$wpc2_default['footer_button_font_color'] = '#a9a9a9';
$wpc2_default['footer_button_font_background_color'] = '#ffffff';
$wpc2_default['footer_form_border_color'] = '#ffffff';
$wpc2_default['footer_form_border_focus_color'] = '#ff4da6';

// Footer
$wpc2_default['footer_copyright'] = 'Site made with ♥ by <a href="http://angiemakes.com">Angie Makes</a>';
$wpc2_default['footer_info_font_color'] = '#827f7f';
$wpc2_default['footer_info_link_font_color'] = '#ff4da6';
$wpc2_default['footer_info_link_font_hover_color'] = '#fb486e';
$wpc2_default['affiliate_username'] = '';

// Colors
$wpc2_default['color_white'] = '#ffffff';
$wpc2_default['color_lightest_gray'] = '#f5f5f5';
$wpc2_default['color_lighter_gray'] = '#dbdbdb';
$wpc2_default['color_light_gray'] = '#999999';
$wpc2_default['color_gray'] = '#555555';
$wpc2_default['color_dark_gray'] = '#333333';
$wpc2_default['color_darker_gray'] = '#222222';
$wpc2_default['color_darkest_gray'] = '#111111';
$wpc2_default['color_black'] = '#000000';
$wpc2_default['color_primary'] = '#ff4da6';
$wpc2_default['color_primary_contrast'] = '#ffffff';
$wpc2_default['color_secondary'] = '#f2cdcb';
$wpc2_default['color_secondary_contrast'] = '#ffffff';
$wpc2_default['color_inverse'] = '#f9f5f4';
$wpc2_default['color_inverse_contrast'] = '#ff4da6';
$wpc2_default['color_success'] = '#5cb85c';
$wpc2_default['color_success_contrast'] = '#efffef';
$wpc2_default['color_warning'] = '#f0ad4e';
$wpc2_default['color_warning_contrast'] = '#fff2e8';
$wpc2_default['color_danger'] = '#d9534f';
$wpc2_default['color_danger_contrast'] = '#ffe8e8';
$wpc2_default['color_info'] = '#5bc0de';
$wpc2_default['color_info_contrast'] = '#eff9ff';
$wpc2_default['color_green'] = '#5f9025';
$wpc2_default['color_green_contrast'] = '#d3e8da';
$wpc2_default['color_yellow'] = '#faeb48';
$wpc2_default['color_yellow_contrast'] = '#695d43';
$wpc2_default['color_blue'] = '#5091b2';
$wpc2_default['color_blue_contrast'] = '#e9f7fe';
$wpc2_default['color_red'] = '#de5959';
$wpc2_default['color_red_contrast'] = '#ffe9e9';

// Favicon
$wpc2_default['favicon'] = '';

// Responsiveness
$wpc2_default['responsive_enabled'] = '1';
$wpc2_default['responsive_menu_width'] = '900';

// Custom Fonts
$wpc2_default['at_font_face'] = "";
/*$wpc2_default['at_font_face'] = trim("
@font-face {
    font-family: 'Garogier';
    src: url('" . get_template_directory_uri() . "/fonts/garogier-webfont.eot');
    src: url('" . get_template_directory_uri() . "/fonts/garogier-webfont.eot?#iefix') format('embedded-opentype'),
         url('" . get_template_directory_uri() . "/fonts/garogier-webfont.woff') format('woff'),
         url('" . get_template_directory_uri() . "/fonts/garogier-webfont.ttf') format('truetype'),
         url('" . get_template_directory_uri() . "/fonts/garogier-webfont.svg#garogierregular') format('svg');
    font-weight: normal;
    font-style: normal;
}
");*/

// Custom CSS
$wpc2_default['custom_css'] = "";

/**
 * Set up dynamic sidebar options. Need to be able to read
 * post types, but needs to be set before customizer
 *
 * Needs to be also set before setting default options
 *
 * @since 3.8.1
 * @access public
 *
 * @return void
 */
function wpcanvas2_set_sidebar_options() {
	global $wpc2_default;

	// Set default options for sidebar display
	$whitelist = array( 'post', 'blog', 'search', 'archive' );
	$page_formats = wpcanvas2_page_formats();
	if ( is_array( $page_formats ) &&   ! empty( $page_formats ) ) {
		foreach ( $page_formats as $key => $value ) {
			$id = 'sidebar_display_' . $key;

			$default = false;
			if ( in_array( $key, $whitelist ) ) {
				$default = true;
			}

			$wpc2_default[ $id ] = $default;

		}
	}
}
add_action( 'init', 'wpcanvas2_set_sidebar_options', 100 );

/**
 * Set default options
 * 
 * wp_loaded gets called before template_redirect, so we can safely set
 * a custom $content_width.
 *
 * Also, if we call get_theme_mod any sooner, then we can't live preview.
 */
function wpcanvas2_default_options() {
	global $wpc2_default;
	global $wpc2;

	foreach ( $wpc2_default as $key => $value ) {
		$wpc2[ $key ] = get_theme_mod( $key, $value );
	}

	$wpc2['content_width'] = $wpc2['site_width'] - $wpc2['sidebar_width'] - $wpc2['sidebar_edge_padding'] - ( 2 * $wpc2['edge_padding'] );
	$wpc2['full_content_width'] = $wpc2['site_width'] - ( 2 * $wpc2['edge_padding'] );
	$wpc2['full_content_border_width'] = $wpc2['site_width'];
}
add_action( 'wp_loaded', 'wpcanvas2_default_options' );

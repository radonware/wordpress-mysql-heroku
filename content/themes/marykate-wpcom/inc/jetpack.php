<?php
/**
 * Jetpack Compatibility File
 * See: http://jetpack.me/
 *
 * @package WPCanvas2
 */

/**
 * Add theme support for Infinite Scroll.
 * See: http://jetpack.me/support/infinite-scroll/
 */
function wpcanvas2_jetpack_setup() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'main',
		'footer'    => 'page',
		'footer_widgets' => true,
	) );
}
add_action( 'after_setup_theme', 'wpcanvas2_jetpack_setup' );

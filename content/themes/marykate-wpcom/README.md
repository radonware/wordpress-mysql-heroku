# WP Canvas 2 #

**Contributors:** cbaldelomar  
**Requires at least:** 3.7  
**Tested up to:** 3.7.1  
**License:** GPLv2 or later  
**License URI:** http://www.gnu.org/licenses/gpl-2.0.html  

A canvas theme for faster WordPress development

## Description ##

WP Canvas 2 is a great starter theme for anyone who wants more control over the design of their website. Baked into this theme are typographic controls that give you the ability to upload any custom web font or Google font and control font styles. WP Canvas also includes a highly customizable panel that lets you control every color on your site, easily change your site’s width and sidebar width, and customize CSS. This theme is also designed to play well with all properly written plugins and widgets.

See WP Canvas 2 in action at [WP Canvas 2](http://webplantmedia.com/starter-themes/wpcanvas2/).

### Professional Support

If you need professional theme support, you can contact me at [Web Plant Media](http://webplantmedia.com/contact/), or by my email chris@webplantmedia.com.

## Changelog ##

### Version 2.65.1

* disabled share button filter feature
* updated instagram style for sidebar and footer
* updated mail chimp widget style
* one click demo import
* changed site title from h1 to div element for seo
* changed widget title from h1 to h2 element
* changed menu toggle button from h1 to div element
* better share button style support for older themes
* Restored default Jetpack share buttons
* Removed 500px width limit for footer widgets
* updated social icons style

### Version 2.49.1

* Updated language pot file
* display title tag using add_theme_support
* Updated WooCommerce style for cart and checkout pages.

### Version 2.45.1

* Update Google Fonts API url so it works with SSL
* Fixed bug with 4 bottom image links not being responsive
* smoother sticky menu when scrolling
* updated gallery js file
* Fixed bug with border color and accent color not being applied to masonry box
* small bug fix with share buttons
* small css bug fix on mobile entry footer
* Better entry footer css for mobile devices
* Better responsive style for share buttons
* fixed style bug with border color not being applied to double borders
* updated color scheme for social font icons
* Added style support for social icons format
* better responsive image links under gallery
* wrapped more function in conditional statement for child theme developers
* fixed style bug with password protect form
* WooCommerce - fixed bad CSS value for onsale box
* fixed image link captions from hiding on mobile devices
* Preparing to add support for multiple character sets for Google Web Fonts
* Updated Google Font list to include newest fonts
* Updated to Font Awesome 4.3.0
* responsive style for comments
* responsive css for comment form.
* responsive css for author description
* updated previous commit different footer width in responsive mode
* fixed widget widths when viewing on iPhone
* updated style on jetpack related posts
* center aligned about me image
* MAYBE always show byilne
* fixed bug with admin meta info not always showing on blog page
* hiding stars and sale price from archiev, related, and featured products.
* fixed style in woocommerce gallery thumbnails
* Disable animation on shortcode tabs element
* Fixed bug with some custom post types not showing is sidebar display
* Fixed bug with search archive not returning correct sidebar display
* Moved transient flush function
* fixed bug with post_thumbnail not being initialized
* fixed bug with Jetpack photon not having padding between excerpt

### Version 2.38.2

* Added icon CSS
* Added opacity change CSS to share buttons
* Added Print Icon

### Version 2.37.1

* updated class name for page template CSS
* Renamed showcase template to no menu template and improved functionality
* added accent color to background of skillbar
* Added content accent color and updated plugin color schemes
* added ability to insert affilate link to footer, also updated footer structure
* updated editor stylesheet
* Firefox v35 fixed select appearance bug. Removed old hack.
* Added alt attribute to logo image in header
* Showcase Header - eFixed viewport on iOS when resopnsiveness is turned off
* Added print stylesheet
* Recalcuate sticky menu data after header images load.
* Sticky menu is disabled when viewing tablets 768px and under
* Fixed viewport on iOS when resopnsiveness is turned off
* Applied testimonial font to content only
* woocommerce style update for red remove button
* Updated autocomplete library version 1.2.16
* Added new Google Fonts to autocomplete selection
* Fixed bug with autocomplete not populating
* fixed at font css generator bug
* Added woff2 font support when upload custom fonts
* fixed bug with height changing in menu bar when using font awesome icons
* added opacity change on hover for social media images in header
* added master id and changed base css for woocomerce override
* add css3 support for multiple background images
* stronger style weight on jetpack comment form checkbox
* removed thin dotted line from firefox focus
* change function name from reduce_font to mul_px
* moved check for post type from share buttons to post meta
* added yellow color to rating starts in woocommerce
* woocommerce gallery image spacing fix
* overflow hidden h3 title ellipses for products
* put yellow color on starts
* nowrap, hide, ellipsis on product title
* Change z-index on LayerSlider arrows
* responsive comments
* updated header showcase
* added snippets folder
* added site padding class
* improved sanitation
* fixed responsive title style for tablet devices
* better animation support
* Added text field sanitize method
* fixed js bug in customizer
* removed red border around Firefox input
* fixed inline css bug
* better cross browser buttons and forms
* show nav button on attachment page
* Accidently removed Jetpack Likes filter. Added it back.
* style support for share button font icons
* fixed style bug with header

### Version 2.29.1.1

* Initial release

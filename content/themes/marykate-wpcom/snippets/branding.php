<?php
/**
 * The template for displaying header logo.
 */
global $wpc2;
?>
<div class="site-branding">
	<?php if ( $wpc2['logo_image'] ) : ?>
	<div class="site-logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo $wpc2['logo_image']; ?>" alt="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>" /></a></div>
	<?php endif; ?>
	<?php if ( $wpc2['show_title'] ) : ?>
		<div class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></div>
	<?php endif; ?>
	<?php if ( $wpc2['show_description'] ) : ?>
		<div class="site-description"><?php bloginfo( 'description' ); ?></div>
	<?php endif; ?>

	<?php if ( $wpc2['header_show_social_icons'] ) : ?>
		<?php wpcanvas2_social_icons(); ?>
	<?php endif; ?>
</div>

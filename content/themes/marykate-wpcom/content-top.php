<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package WPCanvas2
 */
?>

	<div class="entry-content entry-top-content">
		<?php wpcanvas2_the_top_content(); ?>
	</div><!-- .entry-content -->

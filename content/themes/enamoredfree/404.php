<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package enamoredfree
 * @since enamoredfree 1.0
 */

get_header(); ?>

		<div id="primary" class="content-area column full">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php _e( 'Sorry! The page you requested cannot be found.', 'enamoredfree' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php _e( 'It looks like nothing was found at this location. Perhaps searching can help.', 'enamoredfree' ); ?></p>

					<?php get_search_form(); ?>

						</ul>
					</div><!-- .widget -->


				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
<?php
/**
 * The template for displaying Archive pages.
 *
 * @package enamoredfree
 * @since enamoredfree 1.0
 */

get_header(); ?>

	<?php if ( get_theme_mod( 'enamoredfree_archive_layout' ) == 'left' ) : ?>

	<?php get_template_part( 'layouts/content', 'left' ); ?>

	<?php elseif ( get_theme_mod( 'enamoredfree_archive_layout' ) == 'right' ): ?>

	<?php get_template_part( 'layouts/content', 'right' ); ?>

	<?php else : ?>

	<?php get_template_part( 'layouts/content', 'right' ); ?>

	<?php endif; ?>

<?php get_footer(); ?>

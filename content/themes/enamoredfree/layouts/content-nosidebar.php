<?php
/**
 * The template used for displaying content
 *
 * @package enamoredfree
 * @since enamoredfree 1.0
 */
?>

	<div id="primary" class="content-area column full">
		<main id="main" class="site-main" role="main">

			<?php if ( have_posts() ) : ?>

				<?php if (is_archive()) : ?>
				<header class="page-header">
					<h3 class="page-title">
						<?php
							if ( is_category() ) :
								single_cat_title();

							elseif ( is_tag() ) :
								single_tag_title();

							elseif ( is_author() ) :
								printf( __( 'Author: %s', 'enamoredfree' ), '<span class="vcard">' . get_the_author() . '</span>' );

							elseif ( is_day() ) :
								printf( __( 'Day: %s', 'enamoredfree' ), '<span>' . get_the_date() . '</span>' );

							elseif ( is_month() ) :
								printf( __( 'Month: %s', 'enamoredfree' ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format', 'enamoredfree' ) ) . '</span>' );

							elseif ( is_year() ) :
								printf( __( 'Year: %s', 'enamoredfree' ), '<span>' . get_the_date( _x( 'Y', 'yearly archives date format', 'enamoredfree' ) ) . '</span>' );

							elseif ( is_tax( 'post_format', 'post-format-aside' ) ) :
								_e( 'Asides', 'enamoredfree' );

							elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) :
								_e( 'Galleries', 'enamoredfree');

							elseif ( is_tax( 'post_format', 'post-format-image' ) ) :
								_e( 'Images', 'enamoredfree');

							elseif ( is_tax( 'post_format', 'post-format-video' ) ) :
								_e( 'Videos', 'enamoredfree' );

							elseif ( is_tax( 'post_format', 'post-format-quote' ) ) :
								_e( 'Quotes', 'enamoredfree' );

							elseif ( is_tax( 'post_format', 'post-format-link' ) ) :
								_e( 'Links', 'enamoredfree' );

							elseif ( is_tax( 'post_format', 'post-format-status' ) ) :
								_e( 'Statuses', 'enamoredfree' );

							elseif ( is_tax( 'post_format', 'post-format-audio' ) ) :
								_e( 'Audios', 'enamoredfree' );

							elseif ( is_tax( 'post_format', 'post-format-chat' ) ) :
								_e( 'Chats', 'enamoredfree' );

							else :
								_e( 'Latest from', 'enamoredfree' );

							endif;
						?>
					</h3>
					<?php
						$term_description = term_description();
						if ( ! empty( $term_description ) ) :
							printf( '<div class="taxonomy-description">%s</div>', $term_description );
						endif;
					?>
				</header><!-- .page-header -->
				<?php endif; ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<header class="entry-header">		
							<?php if ( 'post' == get_post_type() ) : ?>
							<div class="entry-meta">
								<?php enamoredfree_posted_on(); ?>
								<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
								<span class="comments-link"><?php comments_popup_link( __( 'Leave a Comment', 'enamoredfree' ), __( '1 Comment', 'enamoredfree' ), __( '% Comments', 'enamoredfree' ) ); ?></span>
								<?php endif; ?>
							</div><!-- .entry-meta -->
							<?php endif; ?>

							<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>
							
							<?php if ( has_post_thumbnail() && ! post_password_required() && ! is_attachment() ) : ?>

                                                <div class="entry-thumbnail">
 							<?php the_post_thumbnail( 'featured-image' ); ?>
                                                </div>

							<?php endif; ?>
						</header><!-- .entry-header -->

						<?php if ( get_theme_mod( 'enamoredfree_excerpt' ) == 'yes' ) : ?>
						<div class="entry-summary">
							<?php the_excerpt(); ?>
						</div><!-- .entry-summary -->
						<?php else : ?>
						<div class="entry-content">
							<?php the_content( __( 'Read more', 'enamoredfree' ) ); ?>
							<?php
								wp_link_pages( array(
									'before' => '<div class="page-links">' . __( 'Pages:', 'enamoredfree' ),
									'after'  => '</div>',
								) );
							?>
						</div><!-- .entry-content -->
						<?php endif; ?>

					<footer class="entry-footer">
						<?php if ( 'post' == get_post_type() ) : // Hide category and tag text for pages on Search ?>
							<?php
								$categories_list = get_the_category_list( __( ', ', 'enamoredfree' ) );
								if ( $categories_list ) :
							?>
							<span class="cat-links">
								<?php printf( __( 'Filed Under: %1$s', 'enamoredfree' ), $categories_list ); ?>
							</span>
							<?php endif; // End if categories ?>

							<?php
								$tags_list = get_the_tag_list( '', __( ', ', 'enamoredfree' ) );
								if ( $tags_list ) :
							?>
							<span class="tags-links">
								<?php printf( __( '| Tagged: %1$s', 'enamoredfree' ), $tags_list ); ?>
							</span>
							<?php endif; // End if $tags_list ?>
						<?php endif; // End if 'post' == get_post_type() ?>

						<?php edit_post_link( __( '(Edit)', 'enamoredfree' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-footer -->

					</article><!-- #post-## -->

				<?php endwhile; /* End of the Loop */ ?>

				<nav class="pagination"><?php enamoredfree_paging_nav(); ?></nav>

			<?php else : ?>

				<?php get_template_part( 'content', 'none' ); ?>

			<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

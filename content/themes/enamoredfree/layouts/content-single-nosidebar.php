<?php
/**
 * The template used for displaying single post
 *
 * @package enamoredfree
 * @since enamoredfree 1.0
 */
?>

	<div id="primary" class="content-area column full">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<div class="entry-meta">
							<?php enamoredfree_posted_on(); ?>
						</div><!-- .entry-meta -->
						
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						
						<?php if ( has_post_thumbnail() && ! post_password_required() && ! is_attachment() ) : ?>

						<?php endif; ?>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<?php the_content(); ?>
						<?php
							wp_link_pages( array(
								'before' => '<div class="page-links">' . __( 'Pages:', 'enamoredfree' ),
								'after'  => '</div>',
							) );
						?>
					</div><!-- .entry-content -->

					<footer class="entry-footer">
						<?php if ( 'post' || 'portfolio' == get_post_type() ) : // Hide category and tag text for pages on Search ?>
							<?php
								/* translators: used between list items, there is a space after the comma */
								$categories_list = get_the_category_list( __( ', ', 'enamoredfree' ) );
								if ( $categories_list ) :
							?>
							<span class="cat-links">
								<?php printf( __( 'Filed Under: %1$s', 'enamoredfree' ), $categories_list ); ?>
							</span>
							<?php endif; // End if categories ?>
							
							<?php
								/* translators: used between list items, there is a space after the comma */
								$tags_list = get_the_tag_list( '', __( ', ', 'enamoredfree' ) );
								if ( $tags_list ) :
							?>
							<span class="tags-links">
								<?php printf( __( '| Tagged %1$s', 'enamoredfree' ), $tags_list ); ?>
							</span>
							<?php endif; // End if $tags_list ?>
						<?php endif; // End if 'post' == get_post_type() ?>

						<?php edit_post_link( __( '(Edit)', 'enamoredfree' ), '<span class="edit-link">', '</span>' ); ?>

						<?php get_template_part('share'); ?>
						
					</footer><!-- .entry-footer -->
				</article><!-- #post-## -->

				<?php enamoredfree_post_nav(); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || '0' != get_comments_number() ) :
						comments_template();
					endif;
				?>

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->
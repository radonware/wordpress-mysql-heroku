<?php
/**
 * The Template for displaying all single posts.
 *
 * @package enamoredfree
 * @since enamoredfree 1.0
 */

get_header(); ?>

	<?php if ( get_theme_mod( 'enamoredfree_single_layout' ) == 'left' ) : ?>

	<?php get_template_part( 'layouts/content', 'single-left' ); ?>

	<?php elseif ( get_theme_mod( 'enamoredfree_single_layout' ) == 'nosidebar' ): ?>

	<?php get_template_part( 'layouts/content', 'single-nosidebar' ); ?>

	<?php else : ?>

	<?php get_template_part( 'layouts/content', 'single-right' ); ?>

	<?php endif; ?>

<?php get_footer(); ?>
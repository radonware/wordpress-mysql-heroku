<?php
/**
 * The main template file.
 *
 * @package enamoredfree
 * @since enamoredfree 1.0
 */

get_header(); ?>

	<?php if ( get_theme_mod( 'enamoredfree_layout' ) == 'left' ) : ?>

	<?php get_template_part( 'layouts/content', 'left' ); ?>

	<?php elseif ( get_theme_mod( 'enamoredfree_layout' ) == 'nosidebar' ): ?>

	<?php get_template_part( 'layouts/content', 'nosidebar' ); ?>

	<?php else : ?>

	<?php get_template_part( 'layouts/content', 'right' ); ?>

	<?php endif; ?>

<?php get_footer(); ?>

<?php
/**
 * enamoredfree Theme Customizer
 *
 * @package enamoredfree
 * @since enamoredfree 1.0
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function enamoredfree_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
        $wp_customize->remove_control( 'header_textcolor' ); 
}
add_action( 'customize_register', 'enamoredfree_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function enamoredfree_customize_preview_js() {
	wp_enqueue_script( 'enamoredfree_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'enamoredfree_customize_preview_js' );

/**
 * Customizer CSS
 */
function enamoredfree_enqueue_customizer_controls_styles() {

  wp_register_style( 'enamoredfree-customizer-controls', get_template_directory_uri() . '/inc/customizer-controls.css', NULL, NULL, 'all' );
  wp_enqueue_style( 'enamoredfree-customizer-controls' );

  }

/**
 * Customizer Admin JS
 */
add_action( 'customize_controls_print_styles', 'enamoredfree_enqueue_customizer_controls_styles' );

function enamoredfree_enqueue_customizer_admin_scripts() {

  wp_register_script( 'customizer-admin-js', get_template_directory_uri() . '/js/customizer-admin.js', array( 'jquery' ), NULL, true );
  wp_enqueue_script( 'customizer-admin-js' );

  }

add_action( 'customize_controls_print_footer_scripts', 'enamoredfree_enqueue_customizer_admin_scripts' );

/**
 * Customizer
 *
 * @since enamoredfree 1.0
 */
function enamoredfree_theme_customizer( $wp_customize ) {
/*--------------------------------------------------------------
		Blog Layout
	--------------------------------------------------------------*/
	$wp_customize->add_section( 'enamoredfree_layout_section' , array(
	    'title' => __( 'Blog Layout', 'enamoredfree' ),
	    'priority' => 10,
	    'description' => 'Layout Customization',
	) );

	/* Blog Page */
	$wp_customize->add_setting('enamoredfree_layout', array(
		'default' => 'right',
	));

	$wp_customize->add_control('enamoredfree_layout', array(
		'label' => __('Blog Page Layout', 'enamoredfree'),
		'section' => 'enamoredfree_layout_section',
		'settings' => 'enamoredfree_layout',
		'type' => 'select',
		'choices' => array(
			'right' => 'Right Sidebar',
			'nosidebar' => 'No Sidebar',
		),
	));

	/* Archive Page */
	$wp_customize->add_setting('enamoredfree_archive_layout', array(
		'default' => 'Right Sidebar',
	));

	$wp_customize->add_control('enamoredfree_archive_layout', array(
		'label' => __('Archive Page Layout', 'enamoredfree'),
		'section' => 'enamoredfree_layout_section',
		'settings' => 'enamoredfree_archive_layout',
		'type' => 'select',
		'choices' => array(
			'right' => 'Right Sidebar',
		),
	));

	/* Single Blog Post */
	$wp_customize->add_setting('enamoredfree_single_layout', array(
		'default' => 'right',
	));

	$wp_customize->add_control('enamoredfree_single_layout', array(
		'label' => __('Single Blog Post Layout', 'enamoredfree'),
		'section' => 'enamoredfree_layout_section',
		'settings' => 'enamoredfree_single_layout',
		'type' => 'select',
		'choices' => array(
			'right' => 'Right Sidebar',
			'nosidebar' => 'No Sidebar'
		),
	));

	/* Excerpt Automatic */
	$wp_customize->add_setting('enamoredfree_excerpt', array(
		'default' => 'yes',
	));

	$wp_customize->add_control('enamoredfree_excerpt', array(
		'label' => __('Excerpt Automatic', 'enamoredfree'),
		'section' => 'enamoredfree_layout_section',
		'settings' => 'enamoredfree_excerpt',
		'type' => 'select',
		'choices' => array(
			'no' => 'No',
			'yes' => 'Yes'
		),
	));


	/*--------------------------------------------------------------
		Custom CSS
	--------------------------------------------------------------*/
	class enamoredfree_Customize_Textarea_Control extends WP_Customize_Control {
	    public $type = 'textarea';
	 
	    public function render_content() {
	        ?>
	            <label>
	                <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
	                <textarea rows="5" style="width:100%;" <?php $this->link(); ?>><?php echo esc_textarea( $this->value() ); ?></textarea>
	            </label>
	        <?php
	    }
	}

	$wp_customize->add_section( 'enamoredfree_css_section' , array(
	    'title'       => __( 'Custom CSS', 'enamoredfree' ),
	    'priority'    => 90,
	    'description' => 'You can add your custom CSS',
	) );

	$wp_customize->add_setting( 'enamoredfree_css' );
	 
	$wp_customize->add_control(
	    new enamoredfree_Customize_Textarea_Control(
	        $wp_customize,
	        'enamoredfree_css',
	        array(
	            'label' => 'Custom CSS',
	            'section' => 'enamoredfree_css_section',
	            'settings' => 'enamoredfree_css'
	        )
	    )
	);

	/*--------------------------------------------------------------
		Footer Scripts
	--------------------------------------------------------------*/
	$wp_customize->add_section( 'enamoredfree_scripts_section' , array(
	    'title'       => __( 'Custom Footer Scripts', 'enamoredfree' ),
	    'priority'    => 100,
	    'description' => 'You can add your custom Scripts in the footer without the tag "script". For example: google analytics.',
	) );

	$wp_customize->add_setting( 'enamoredfree_scripts' );
	 
	$wp_customize->add_control(
	    new enamoredfree_Customize_Textarea_Control(
	        $wp_customize,
	        'enamoredfree_scripts',
	        array(
	            'label' => 'Custom Scripts',
	            'section' => 'enamoredfree_scripts_section',
	            'settings' => 'enamoredfree_scripts'
	        )
	    )
	);
}
add_action('customize_register', 'enamoredfree_theme_customizer');

/**
 * Customizer Apply Style
 *
 * @since enamoredfree 1.0
 */
if ( ! function_exists( 'enamoredfree_apply_style' ) ) :
  	
  	function enamoredfree_apply_style() {	
		if ( get_theme_mod('enamoredfree_slider_color') || 

			 get_theme_mod('enamoredfree_css')
		) { 
		?>
				
			</style>
			<style id="enamoredfree-custom-css">
				<?php if ( get_theme_mod('enamoredfree_css') ) : ?>
					<?php echo get_theme_mod('enamoredfree_css');  ?>;
				<?php endif; ?>
			</style>
		<?php
    }
}
endif;
add_action( 'wp_head', 'enamoredfree_apply_style' );

/**
 * Customizer Footer
 *
 * @since enamoredfree 1.0
 */
if ( ! function_exists( 'enamoredfree_apply_footer' ) ) :
  	
  	function enamoredfree_apply_footer() {	
		if ( get_theme_mod('enamoredfree_scripts') ) { 
		?>

		<script id="enamoredfree-custom-scriptss">
			<?php if ( get_theme_mod('enamoredfree_scripts') ) : ?>
				<?php echo get_theme_mod('enamoredfree_scripts');  ?>;
			<?php endif; ?>
		</script>
		<?php
    }
}
endif;
add_action('wp_footer', 'enamoredfree_apply_footer');
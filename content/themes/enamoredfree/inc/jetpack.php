<?php
/**
 * Jetpack Compatibility File
 * See: http://jetpack.me/
 *
 * @package enamoredfree
 * @since enamoredfree 1.0
 */

/**
 * Add theme support for Infinite Scroll.
 * See: http://jetpack.me/support/infinite-scroll/
 */
function enamoredfree_jetpack_setup() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'main',
		'footer'    => 'page',
	) );
}
add_action( 'after_setup_theme', 'enamoredfree_jetpack_setup' );

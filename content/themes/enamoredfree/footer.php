<?php
/**
 * The template for displaying the footer.
 *
 * @package enamoredfree
 * @since enamoredfree 1.0
 */
?>

		</div><!-- #content -->

	</div><!-- .container -->

	<footer id="colophon" class="site-footer" role="contentinfo">

		<div class="container">

			<div class="site-info">

				<?php get_sidebar('footer'); ?>

				<?php if ( get_theme_mod( 'enamoredfree_footer' ) ) : ?>
					
					<?php echo get_theme_mod( 'enamoredfree_footer' ); ?>
				
				<?php else : ?>
					
					Designed with love by <a rel="nofollow" href="https://beautifuldawndesigns.net/" title="Beautiful Dawn Designs">Beautiful Dawn Designs</a>
				
				<?php endif; ?>

			</div><!-- .site-info -->

		</div><!-- .container -->
		
	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

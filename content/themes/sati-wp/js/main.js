jQuery(document).ready(function () {

    $percentDone = jQuery('.percent-complete span').text();
    jQuery('.percent-complete span').text('0');

    var ua = navigator.userAgent.toLowerCase();
    if ((ua.indexOf("safari/") !== -1 && ua.indexOf("windows") !== -1 && ua.indexOf("chrom") === -1) || is_touch_device())
    {
        jQuery("html").css('overflow', 'auto');
    } else
    {
        jQuery("html").niceScroll({cursorcolor: "#b1b1b1", scrollspeed: 100, mousescrollstep: 80, cursorwidth: "12px", cursorborder: "none", cursorborderradius: "0px"});
    }


    //Turn off autocomplete for subscribe form
    jQuery('.subscribe-form form').attr('autocomplete', 'off');

    //Fix for subscribe form
    jQuery(".subscribe-form .wpcf7-response-output").before('<div class="clear"></div>');


    fixSubscribeMask();


    //Placeholder show/hide
    jQuery('input, textarea').focus(function () {
        jQuery(this).data('placeholder', jQuery(this).attr('placeholder'));
        jQuery(this).attr('placeholder', '');
    });
    jQuery('input, textarea').blur(function () {
        jQuery(this).attr('placeholder', jQuery(this).data('placeholder'));
    });


    //On start, show Front Section if has no hash in URL    
    var hash = location.hash;
    hash = hash.split('#');
    hash = hash[1];
    if (hash != '')
    {
        if (jQuery('#' + hash).length)
        {
            jQuery('#' + hash).addClass('in-focus');
            jQuery('.toggle-holder').addClass('in-focus');
        } else
        {
            jQuery(".section.front-page").addClass('in-focus');
        }
    }

    //Show-Hide Section    
    jQuery('a.scroll').click(function (e) {
        e.preventDefault();
        var section = jQuery(this).attr('href');
        section = section.split('#');
        section = section[1];

        if (jQuery('#' + section).length)
        {
            jQuery('.toggle-holder').addClass('in-focus');

            jQuery(".section").each(function () {
                if (jQuery(this).attr('id') !== section)
                {
                    jQuery(this).removeClass('in-focus');
                } else
                {
                    jQuery(this).addClass('in-focus');
                }
            });
        }
    });


    //Show-Hide Front Section
    jQuery('.toggle-holder').click(function () {
        jQuery(".section").removeClass('in-focus');
        jQuery(".section.front-page").addClass('in-focus');
        jQuery("html").getNiceScroll().resize();
    });

    //Fix for default menu
    jQuery('.default-menu ul').addClass('main-menu sm sm-clean');

});



jQuery(window).load(function () {

//Set menu
    jQuery('.main-menu').smartmenus({
        subMenusSubOffsetX: 1,
        subMenusSubOffsetY: -8,
        markCurrentItem: true
    });
    var $mainMenu = jQuery('.main-menu').on('click', 'span.sub-arrow', function (e) {
        var obj = $mainMenu.data('smartmenus');
        if (obj.isCollapsible()) {
            var $item = jQuery(this).parent(),
                    $sub = $item.parent().dataSM('sub');
            $sub.dataSM('arrowClicked', true);
        }
    }).bind({
        'beforeshow.smapi': function (e, menu) {
            var obj = $mainMenu.data('smartmenus');
            if (obj.isCollapsible()) {
                var $menu = jQuery(menu);
                if (!$menu.dataSM('arrowClicked')) {
                    return false;
                }
                $menu.removeDataSM('arrowClicked');
            }
        }
    });


    //PrettyPhoto initial
    jQuery('a[data-rel]').each(function () {
        jQuery(this).attr('rel', jQuery(this).data('rel'));
    });
    jQuery("a[rel^='prettyPhoto']").prettyPhoto({
        animation_speed: 'fast', /* fast/slow/normal */
        slideshow: false, /* false OR interval time in ms */
        autoplay_slideshow: false, /* true/false */
        opacity: 0.80, /* Value between 0 and 1 */
        show_title: true, /* true/false */
        allow_resize: true, /* Resize the photos bigger than viewport. true/false */
        default_width: 500,
        default_height: 344,
        counter_separator_label: '/', /* The separator for the gallery counter 1 "of" 2 */
        theme: 'pp_default', /* light_rounded / dark_rounded / light_square / dark_square / facebook */
        hideflash: false, /* Hides all the flash object on a page, set to TRUE if flash appears over prettyPhoto */
        wmode: 'opaque', /* Set the flash wmode attribute */
        autoplay: true, /* Automatically start videos: True/False */
        modal: false, /* If set to true, only the close button will close the window */
        overlay_gallery: false, /* If set to true, a gallery will overlay the fullscreen image on mouse over */
        keyboard_shortcuts: true, /* Set to false if you open forms inside prettyPhoto */
        deeplinking: false,
        social_tools: false,
        markup: '<div class="pp_pic_holder"> \
						<div class="ppt">&nbsp;</div> \
						<div class="pp_top"> \
							<div class="pp_left"></div> \
							<div class="pp_middle"></div> \
							<div class="pp_right"></div> \
						</div> \
						<div class="pp_content_container"> \
							<div class="pp_left"> \
							<div class="pp_right"> \
								<div class="pp_content"> \
									<div class="pp_loaderIcon"></div> \
									<div class="pp_fade"> \
										<a href="#" class="pp_expand" title="Expand the image">Expand</a> \
										<div class="pp_hoverContainer"> \
											<a class="pp_next" href="#">next</a> \
											<a class="pp_previous" href="#">previous</a> \
										</div> \
										<div id="pp_full_res"></div> \
										<div class="pp_details"> \
											<div class="pp_nav"> \
												<a href="#" class="pp_arrow_previous"><img src="' + ajax_var.base_url + '/images/nav_left.svg" alt=""></a> \
												<p class="currentTextHolder">0/0</p> \
												<a href="#" class="pp_arrow_next"><img src="' + ajax_var.base_url + '/images/nav_right.svg" alt=""></a> \
											</div> \
											<p class="pp_description"></p> \
											{pp_social} \
											<a class="pp_close" href="#"><img src="' + ajax_var.base_url + '/images/close.svg" alt=""></a> \
										</div> \
									</div> \
								</div> \
							</div> \
							</div> \
						</div> \
						<div class="pp_bottom"> \
							<div class="pp_left"></div> \
							<div class="pp_middle"></div> \
							<div class="pp_right"></div> \
						</div> \
					</div> \
					<div class="pp_overlay"></div>',
        iframe_markup: '<iframe src ="{path}" width="{width}" height="{height}" frameborder="no" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'
    });

    var $videoDefaultWidth = Math.ceil(jQuery('body').width() * 0.7);
    var $videoDefaultHeight = Math.ceil($videoDefaultWidth * 0.5625);

    jQuery("a[rel^='prettyPhoto']").each(function () {

        var str = jQuery(this).attr('href');
        if ((str.indexOf("youtube") >= 0 || (str.indexOf("vimeo")) >= 0))
        {
            jQuery(this).attr("href", str + "&width=" + $videoDefaultWidth + "&height=" + $videoDefaultHeight);
        }
    });


    commentFormWidthFix();


    if (jQuery(".subscribe-form .wpcf7").length)
    {
        jQuery(".subscribe-form .wpcf7 input[type=submit]").hover(
                function () {
                    jQuery('.right-mask').addClass("hover");
                },
                function () {
                    jQuery('.right-mask').removeClass("hover");
                }
        );
    }


    if (jQuery(".percent-complete").length)
    {
        var percentPos = jQuery(".percent-complete").offset();
        var windowHeight = jQuery(window).height();

        if (percentPos.top < windowHeight)
        {
            jQuery('.percent-complete').addClass('done');
            jQuery('.percent-txt').css('opacity', '1');
            jQuery('.percent-complete span').each(function () {
                var $this = jQuery(this);
                jQuery({Counter: 0}).animate({Counter: $percentDone}, {
                    duration: 6000,
                    easing: 'easeInOutElastic',
                    step: function () {
                        $this.text(Math.ceil(this.Counter));
                    }
                });
            });
        }

        jQuery(window).scrollTop(1);
        jQuery(window).scrollTop(0);

        jQuery('.percent-complete').one('inview', function (event, visible) {
            if (visible === true) {
                if (!jQuery(this).hasClass('done'))
                {
                    jQuery('.percent-txt').css('opacity', '1');
                    jQuery('.percent-complete span').each(function () {
                        var $this = jQuery(this);
                        jQuery({Counter: 0}).animate({Counter: $percentDone}, {
                            duration: 6000,
                            easing: 'easeInOutElastic',
                            step: function () {
                                $this.text(Math.ceil(this.Counter));
                            }
                        });
                    });
                }
            }
        });
    }

    jQuery('.doc-loader').fadeOut('fast');
});

jQuery(window).resize(function () {
    fixSubscribeMask();
    commentFormWidthFix();

});
//------------------------------------------------------------------------
//Helper Methods -->
//------------------------------------------------------------------------

var commentFormWidthFix = function () {
    jQuery('#commentform #comment, #commentform #email, #commentform #author').innerWidth(jQuery('#commentform').width());
};

function fixSubscribeMask() {
    if (jQuery(".subscribe-form .wpcf7").length)
    {
        var position = jQuery(".subscribe-form .wpcf7-email").offset();
        var postionRight = jQuery(".subscribe-form .wpcf7-email").width() + position.left;
        jQuery(".left-mask").width(postionRight + 2);
        jQuery(".right-mask").width(jQuery('body').width() - postionRight);
    }else
    {
        jQuery(".left-mask, .right-mask").remove();        
    }
}

function is_touch_device() {
    return !!('ontouchstart' in window);
}
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>        
        <meta charset="<?php bloginfo('charset'); ?>" />        
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />  		
        <!--[if lt IE 9]>
        <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/html5.js"></script>
        <![endif]-->              
        <?php wp_head(); ?>
    </head>

    <?php $colorShmemeMode = get_theme_mod('color_scheme', 'light'); ?>

    <body <?php body_class($colorShmemeMode); ?>>
        <?php
        if (!isset($content_width))
            $content_width = 1330;
        ?>

        <?php if (get_option('sati_preloader') !== ''): ?>           
            <table class="doc-loader">
                <tr>
                    <td>
                        <img src=<?php echo get_option('sati_preloader', get_template_directory_uri() . '/images/ajax-document-loader.gif'); ?> alt="<?php echo __('Loading...', 'sati-wp'); ?>" />                    
                    </td>
                </tr>
            </table>   
        <?php endif; ?>      
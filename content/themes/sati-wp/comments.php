<?php if ($comments) : ?>  
    <div class="block center-relative">
        <ol>
            <li>
                <p class="comments-number"><?php comments_number(__('None', 'sati-wp')); ?>. <?php echo __('So far, so good!', 'sati-wp'); ?></p>
            </li>        
            <?php wp_list_comments(array('max_depth' => 4, 'avatar_size' => 48, 'callback' => 'dry_theme_comment')); ?>  
        </ol>

        <div id="comments-pagination">&nbsp;</div>
    </div>
    <div class="comment-form-holder">
        <div class="block center-relative">            
        <?php else : ?>   
            <div class="comment-form-holder">
                <div class="block center-relative">   
                    <span class="comment-separator no-comments block"><?php echo __('No comments yet, be the first one!', 'sati-wp'); ?></span>                       
                <?php endif; ?>  
                <?php
                if (!isset($aria_req)) {
                    $aria_req = '';
                }
                comment_form(
                        array('fields' => array(
                                'author' => '<p class="comment-form-author">' .
                                '<input id="author" placeholder="' . __('NAME', 'sati-wp') . '" name="author" type="text" value="' . esc_attr($commenter['comment_author']) . '" size="20"' . $aria_req . ' /></p>',
                                'email' => '<p class="comment-form-email">' .
                                '<input id="email" placeholder="' . __('EMAIL', 'sati-wp') . '" name="email" type="text" value="' . esc_attr($commenter['comment_author_email']) . '" size="20"' . $aria_req . ' /></p>'
                            ),
                            'comment_field' => '<p class="comment-form-comment"><textarea id="comment" placeholder="' . __('COMMENT', 'sati-wp') . '" name="comment" cols="45" rows="12" aria-required="true"></textarea></p>',
                            'title_reply' => '',
                            'title_reply_to' => 'Leave a reply to %s - ',
                            'comment_notes_before' => '',
                            'comment_notes_after' => '',
                            'label_submit' => __('PUBLISH COMMENT', 'sati-wp')));

                function dry_theme_comment($comment, $args, $depth) {
                    $GLOBALS['comment'] = $comment;
                    ?>
                    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
                        <div id="comment-<?php comment_ID(); ?>" class="single-comment-holder">
                            <?php if ($comment->comment_approved == '0') : ?>
                                <em><?php echo __('Your comment is awaiting moderation.', 'sati-wp'); ?></em>
                                <br /> <br />
                            <?php endif; ?>

                            <?php
                            $get_comment_ID = get_comment_ID();
                            $comment_id = get_comment($get_comment_ID);
                            $parent_comment_id = $comment_id->comment_parent;
                            if ($parent_comment_id != 0) {
                                $get_parent_author_name = get_comment_author($parent_comment_id);
                            }
                            ?>

                            <div class="left vcard">
                                <?php
                                if ($args['avatar_size'] != 0)
                                    echo get_avatar($comment, 70);
                                ?>
                            </div>
                            <div class="comment-right-holder">
                                <ul class="comment-author-date-replay-holder">
                                    <li class="comment-author">
                                        <?php echo comment_author(); ?>
                                    </li>
                                    <li class="comment-reply">
                                        <?php comment_reply_link(array_merge($args, array('add_below' => '', 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                                    </li> 
                                </ul>
                                <p class="comment-date">
                                    <?php comment_date('M j, Y g:i a'); ?>
                                </p>				
                                <div class="comment-text">
                                    <?php
                                    if ($parent_comment_id != 0) {
                                        echo '<span class="replay-at-author">@' . $get_parent_author_name . '</span>';
                                    } comment_text();
                                    ?>
                                </div>			
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear">&nbsp;</div>       

                    <?php } ?>
            </div></div>
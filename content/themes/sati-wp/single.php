<?php get_header(); ?>

<header class = "header-holder center-relative relative">
    <div class = "menu-wraper center-relative">
        <?php
        if (has_nav_menu("custom_menu")) {
            wp_nav_menu(
                    array(
                        "container" => "nav",
                        "container_class" => "big-menu",
                        "container_id" => "header-main-menu",
                        "fallback_cb" => false,
                        "menu_class" => "main-menu sm sm-clean",
                        "theme_location" => "custom_menu",
                        "items_wrap" => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        "walker" => new Sati_Header_Menu()
                    )
            );
        } else {
            echo '<nav id="header-main-menu" class="big-menu">';
            wp_page_menu(array('menu_class' => 'default-menu', 'depth' => '3'));
            echo '</nav>';
        }
        ?>
        <div class="clear"></div>
    </div>                       
</header>

<div id="content" class="site-content center-relative">
    <?php
    if (have_posts()) :
        while (have_posts()) : the_post();
            ?>		

            <article id="post-<?php the_ID(); ?>" <?php post_class('content-770 center-relative'); ?>>                
                <div class="wrapper">                 
                    <div class="entry-content-holder">
                        <?php if (has_post_thumbnail()): ?>
                            <div class="single-post-featured-image">
                                <?php the_post_thumbnail(); ?>
                            </div>
                        <?php endif; ?>
                        <h1 class="entry-title"><?php the_title(); ?></h1>
                        <div class="post-info">
                            <div class="post-date">
                                <?php
                                echo '<span>' . get_the_date('Y/m/d') . '</span>';
                                ?>
                            </div>
                            <div class="cat-links">                                
                                <ul>
                                    <?php
                                    foreach ((get_the_category()) as $category) {
                                        echo '<li><a href="' . get_category_link($category->term_id) . '">' . $category->name . '</a></li>';
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                        <div class="center-relative clear">                  
                            <div class="entry-content">
                                <div class="content-wrap">
                                    <?php
                                    the_content();
                                    if (has_tag()):
                                        ?>	
                                        <div class="tags-holder center-relative">
                                            <?php the_tags('', ''); ?>
                                        </div>
                                        <div class="clear"></div>
                                        <?php
                                    endif;
                                    $defaults = array(
                                        'before' => '<p class="wp-link-pages top-50"><span>' . __('Pages:', 'sati-wp') . '</span>',
                                        'after' => '</p>'
                                    );
                                    wp_link_pages($defaults);
                                    ?>
                                </div>
                            </div>                   
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="nav-links">                
                    <?php
                    $prev_post = get_previous_post();
                    if (is_a($prev_post, 'WP_Post')):
                        ?>
                        <div class="nav-previous">                                                     
                            <p class="nav-previous-text"><a href="<?php echo $prev_post->guid; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/left.png" alt="<?php echo __('Previous', 'sati-wp'); ?>" /></a></p>
                            <div class="clear"></div>
                        </div>
                    <?php endif; ?>
                    <?php
                    $next_post = get_next_post();
                    if (is_a($next_post, 'WP_Post')):
                        ?>                
                        <div class="nav-next">                                                    
                            <p class="nav-next-text"><a href="<?php echo $next_post->guid; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/right.png" alt="<?php echo __('Next', 'sati-wp'); ?>" /></a></p>
                            <div class="clear"></div>
                        </div>
                    <?php endif; ?>
                    <div class="clear"></div>
                </div>
                <?php if (comments_open()) : ?>
                    <div id="comments" class="comments-holder center-relative">	

                        <?php comments_template(); ?>                    

                        <div class="comments-pagination-wrapper">
                            <div class="comments-pagination">
                                <?php paginate_comments_links(array('prev_text' => '&laquo;', 'next_text' => '&raquo;')); ?> 
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <?php
                endif;
                ?> 
                <div class = "clear"></div>
            </article>
            <?php
        endwhile;
    endif;
    ?>    
</div>
<?php get_footer(); ?>
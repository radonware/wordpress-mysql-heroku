<?php
/*
  Template Name: FrontPage
 */
?>

<?php get_header(); ?>

<?php
//FRONTPAGE CONTENT            
if (have_posts()) :
    while (have_posts()) : the_post();

        $classes = array(
            'section',
            'front-page'
        );
        $curentPostID = $post->ID;
        $pageBackground = get_post_meta($curentPostID, "page_background_img", true);
        $percentDone = get_post_meta($curentPostID, "page_percent", true);
        $percentTxt = get_post_meta($curentPostID, "page_percent_txt", true);

        if ($pageBackground != ''):
            ?>       
            <article <?php post_class($classes); ?>  style="background-image:url(<?php echo $pageBackground; ?>); 
                                                     background-repeat: <?php echo get_post_meta($curentPostID, "page_img_repeat", true); ?>; 
                                                     background-position: <?php echo get_post_meta($curentPostID, "page_img_position", true); ?>;                                                  
                                                     ">                        
                                                     <?php else: ?>
                <article <?php post_class($classes); ?>>                            
                <?php endif; ?>
                <header class="header-holder center-relative relative">
                    <div class="menu-wraper center-relative">                
                        <?php
                        if (has_nav_menu("custom_menu")) {
                            wp_nav_menu(
                                    array(
                                        "container" => "nav",
                                        "container_class" => "big-menu",
                                        "container_id" => "header-main-menu",
                                        "fallback_cb" => false,
                                        "menu_class" => "main-menu sm sm-clean",
                                        "theme_location" => "custom_menu",
                                        "items_wrap" => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                        "walker" => new Sati_Header_Menu()
                                    )
                            );
                        } else {
                            echo '<nav id="header-main-menu" class="big-menu">';
                            wp_page_menu(array('menu_class' => 'default-menu', 'depth' => '3'));
                            echo '</nav>';
                        }
                        ?>
                        <div class="clear"></div>
                    </div>
                    <div class="header-logo">
                        <?php if (get_option('sati_header_logo') !== ''): ?>                         
                            <a href="<?php echo(site_url('/')); ?>">
                                <img src="<?php echo get_option('sati_header_logo', get_template_directory_uri() . '/images/satiDefaultLogo.png'); ?>" alt="<?php echo get_bloginfo('name'); ?>" />
                            </a>               
                        <?php endif; ?>                   
                    </div>

                    <div class="clear"></div>	
                </header>

                <div class="block content-1170 center-relative entry-content">               
                    <?php the_content(); ?> 
                </div>

                <div class="subscribe-form">         
                    <div class="block content-1170 center-relative relative">
                        <div class="percent-complete">
                            <span>
                                <?php
                                if ($percentDone != '') {
                                    echo $percentDone;
                                }
                                ?>
                            </span>%</div>
                        <div class="percent-txt"><?php
                            if ($percentTxt != '') {
                                echo $percentTxt;
                            }
                            ?></div>
                    </div>
                    <div class="block content-1170 center-relative">
                        <?php
                        if (function_exists('wpcf7')) {
                            echo do_shortcode('[contact-form-7 title="Subscribe"]');
                        }
                        ?>
                    </div>
                    <div class="left-mask"></div>
                    <div class="right-mask"></div>
                </div>
            </article>


            <?php
        endwhile;
    endif;

    wp_reset_query();

    $page_for_posts = get_option('page_for_posts');
    $args = array('post_type' => 'page', 'posts_per_page' => '-1', 'orderby' => 'menu_order', 'order' => 'ASC');
    $loop = new WP_Query($args);
    if ($loop->have_posts()) :
        while ($loop->have_posts()) : $loop->the_post();

            $slug = $post->post_name;

            if ('frontpage.php' != get_page_template_slug($post->ID)) {
                $classes = array(
                    'section',
                    'regular'
                );

                // CHECK IF IS PAGE INCLUDED IN FRONTPAGE - include all page exept FronPage and Blog page
                if (get_post_meta($post->ID, 'page_structure', true) != 2) {
                    if ($post->ID != $page_for_posts) {
                        ?>    
                        <article id="<?php echo $slug; ?>" <?php post_class($classes); ?>>                            
                            <div class="block content-1170 center-relative content-wrapper">   
                                <div class="toggle-holder absolute">
                                    <div id="toggle">
                                        <div class="first-menu-line"></div>
                                        <div class="second-menu-line"></div>
                                    </div>
                                </div>
                                <h2 class="entry-title"><?php the_title(); ?></h2>
                                <?php the_content(); ?> 
                            </div>
                        </article>
                        <?php
                    }
                }
            }
        endwhile;
    endif;
    ?>    

    <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar(1)) : ?><?php endif; ?>    
    <?php get_footer(); ?>
<?php get_header(); ?>

<header class = "header-holder center-relative relative">
    <div class = "menu-wraper center-relative">
        <?php
        if (has_nav_menu("custom_menu")) {
            wp_nav_menu(
                    array(
                        "container" => "nav",
                        "container_class" => "big-menu",
                        "container_id" => "header-main-menu",
                        "fallback_cb" => false,
                        "menu_class" => "main-menu sm sm-clean",
                        "theme_location" => "custom_menu",
                        "items_wrap" => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        "walker" => new Sati_Header_Menu()
                    )
            );
        } else {
            echo '<nav id="header-main-menu" class="big-menu">';
            wp_page_menu(array('menu_class' => 'default-menu', 'depth' => '3'));
            echo '</nav>';
        }
        ?>
        <div class="clear"></div>
    </div>                       
</header>
<?php
if (have_posts()) :
    while (have_posts()) : the_post();
        ?>
        <article <?php post_class(); ?>>                            
            <div class="block content-1170 center-relative">   
                <h1 class="entry-title"><?php the_title(); ?></h1>
                <?php the_content(); ?> 
            </div>
        </article>      <?php
    endwhile;
endif;
?>    

<?php get_footer(); ?>
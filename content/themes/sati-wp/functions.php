<?php
// <editor-fold defaultstate="collapsed" desc="PO files location">
load_theme_textdomain('sati-wp', get_template_directory() . '/languages');

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Setup theme">
function sati_theme_setup() {

    add_action('wp_enqueue_scripts', 'sati_load_scripts_and_style');
    add_theme_support('post-thumbnails', array('post'));
    add_action('add_meta_boxes', 'sati_add_page_custom_meta_box');
    add_action('save_post', 'sati_save_page_custom_meta');
    add_filter("the_content", "the_content_filter");
    add_action('widgets_init', 'sati_widgets_init', 11);

    add_theme_support('title-tag');

    include('admin/custom-admin.php');
    include('admin/page-custom-admin.php');

    if (function_exists('automatic-feed-links')) {
        add_theme_support('automatic-feed-links');
    }

    add_filter('get_the_archive_title', 'my_theme_archive_title');
    add_filter('wp_die_handler', 'sati_get_dry_die_handler');
    add_action('init', 'sati_register_menu');


    if (current_theme_supports('custom-header')) {
        $default_custom_header_settings = array(
            'default-image' => '',
            'random-default' => false,
            'width' => 0,
            'height' => 0,
            'flex-height' => false,
            'flex-width' => false,
            'default-text-color' => '',
            'header-text' => true,
            'uploads' => true,
            'wp-head-callback' => '',
            'admin-head-callback' => '',
            'admin-preview-callback' => '',
        );
        add_theme_support('custom-header', $default_custom_header_settings);
    }

    if (current_theme_supports('custom-background')) {
        $default_custom_background_settings = array(
            'default-color' => '',
            'default-image' => '',
            'wp-head-callback' => '_custom_background_cb',
            'admin-head-callback' => '',
            'admin-preview-callback' => ''
        );
        add_theme_support('custom-background', $default_custom_background_settings);
    }


    /**
     * Include the TGM_Plugin_Activation class.
     */
    require_once dirname(__FILE__) . '/admin/class-tgm-plugin-activation.php';
    add_action('tgmpa_register', 'my_theme_register_required_plugins');
}

add_action('after_setup_theme', 'sati_theme_setup');

//</editor-fold>
// <editor-fold defaultstate="collapsed" desc="Load CSS and JS">
function sati_load_scripts_and_style() {
    wp_register_style('google_fonts_style', 'http://fonts.googleapis.com/css?family=Montserrat:400,700|Lato:300,400,700');
    wp_enqueue_style('google_fonts_style');

//Initialize once to optimize number of cals to get template directory url method
    $base_theme_url = get_template_directory_uri();

//register and load styles which is used on every pages       
    wp_register_style('clear_style', $base_theme_url . '/css/clear.css');
    wp_enqueue_style('clear_style');
    wp_register_style('font_awesome', $base_theme_url . '/css/font-awesome.min.css');
    wp_enqueue_style('font_awesome');
    wp_register_style('animated-headline', $base_theme_url . '/css/animated-headline.css');
    wp_enqueue_style('animated-headline');
    wp_register_style('carouFredSel_style', $base_theme_url . '/css/carouFredSel.css');
    wp_enqueue_style('carouFredSel_style');
    wp_register_style('prettyPhoto_style', $base_theme_url . '/css/prettyPhoto.css');
    wp_enqueue_style('prettyPhoto_style');
    wp_register_style('common_style', $base_theme_url . '/css/common.css');
    wp_enqueue_style('common_style');
    wp_register_style('sm_cleen', $base_theme_url . '/css/sm-clean.css');
    wp_enqueue_style('sm_cleen');
    wp_register_style('main_theme_style', $base_theme_url . '/style.css');
    wp_enqueue_style('main_theme_style');

    $colorShmemeMode = get_theme_mod('color_scheme', 'light');

    if ($colorShmemeMode == 'dark') {
        wp_register_style('main_dark_theme_style', $base_theme_url . '/style-dark.css');
        wp_enqueue_style('main_dark_theme_style');
    }

    if (is_customize_preview()) {
        wp_register_style('customize_custom_style', $base_theme_url . '/css/customizer.css');
        wp_enqueue_style('customize_custom_style');
    }


//JavaScript


    wp_enqueue_script('jquery', '', '', '', '', true);

    wp_enqueue_script('smartmenus', $base_theme_url . '/js/jquery.smartmenus.min.js', array('jquery'), false, true);
    wp_enqueue_script('inview', $base_theme_url . '/js/jquery.inview.js', array('jquery'), '', true);
    wp_enqueue_script('jquery.mousewheel.min', $base_theme_url . '/js/jquery.mousewheel.min.js', array('jquery'), false, true);
    wp_enqueue_script('jquery.easing.1.3', $base_theme_url . '/js/jquery.easing.1.3.js', array('jquery'), false, true);
    wp_enqueue_script('nicescroll', $base_theme_url . '/js/jquery.nicescroll.min.js', array('jquery'), false, true);
    wp_enqueue_script('jquery.prettyPhoto', $base_theme_url . '/js/jquery.prettyPhoto.js', array('jquery'), '', true);
    wp_enqueue_script('main-headline', $base_theme_url . '/js/main-headline.js', array('jquery'), false, true);
    wp_enqueue_script('main', $base_theme_url . '/js/main.js', array('jquery'), false, true);

    if (is_singular()) {
        if (get_option('thread_comments')) {
            wp_enqueue_script('comment-reply');
        }
    }

    wp_localize_script('main', 'ajax_var', array(
        'base_url' => $base_theme_url
    ));
}

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Custom Excerpt read more">
function the_excerpt_max_charlength($charlength) {
    $excerpt = get_the_excerpt();
    $charlength++;
    global $post;

    if (mb_strlen($excerpt) > $charlength) {
        $subex = mb_substr($excerpt, 0, $charlength - 5);
        $exwords = explode(' ', $subex);
        $excut = - ( mb_strlen($exwords[count($exwords) - 1]) );
        if ($excut < 0) {
            $return = mb_substr($subex, 0, $excut);
        } else {
            $return = $subex;
        }
        $return .= '... <p class="read-more"><a href="' . get_permalink($post->ID) . '">' . __('Read More', 'sati-wp') . '</a></p>';
    } else {
        $return = $excerpt . '<p class="read-more"><a href="' . get_permalink($post->ID) . '">' . __('Read More', 'sati-wp') . '</a></p>';
    }

    return $return;
}

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Portfolio Holder">
function sati_portfolio_holder($atts, $content = null) {

    $return = '<div class="portfolio-holder">';
    $return .= do_shortcode($content);
    $return .= '<div class="clear"></div></div>';

    return $return;
}

add_shortcode("portfolio_holder", "sati_portfolio_holder");

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Portfolio Item">
function sati_portfolio($atts, $content = null) {
    extract(shortcode_atts(array(
        "class" => '',
        "alt" => '',
        "id" => '1',
        "class" => '',
        "target" => '_self',
        "link" => get_template_directory_uri() . '/images/no-photo.png',
        "exlink" => '',
        "thumb" => get_template_directory_uri() . '/images/no-photo.png',
                    ), $atts));

    if ($exlink != '') {
        $return = '<div class="portfolio-item ' . $class . '">
               <a href="' . $exlink . '" target="' . $target . '">
               <img src = "' . $thumb . '" alt = "' . $alt . '" />
               <div class="portfolio-text-holder">               
               <img src="' . get_template_directory_uri() . '/images/icon_view.svg' . '" alt="">               
               </div>
               </a>
               </div>';
    } else {
        $return = '<div class="portfolio-item ' . $class . '">
               <a href="' . $link . '" data-rel="prettyPhoto[gallery1]">
               <img src = "' . $thumb . '" alt = "' . $alt . '" />
               <div class="portfolio-text-holder">               
               <img src="' . get_template_directory_uri() . '/images/icon_view.svg' . '" alt="">               
               </div>
               </a>
               </div>';
    }

    return $return;
}

add_shortcode("portfolio", "sati_portfolio");

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Add the Meta Box to 'Pages'"> 
function sati_add_page_custom_meta_box() {
    add_meta_box(
            'page_custom_meta_box', // $id  
            __('Page Preference', 'sati-wp'), // $title   
            'sati_show_page_custom_meta_box', // $callback  
            'page', // $page  
            'normal', // $context  
            'high'); // $priority     
}

// Field Array Post Page 
$prefix = 'page_';
$page_custom_meta_fields = array(
    array(
        'label' => __('Site is done (percent)', 'sati-wp'),
        'desc' => '',
        'id' => $prefix . 'percent',
        'type' => 'text'
    ),
    array(
        'label' => __('Site is done (small text)', 'sati-wp'),
        'desc' => '',
        'id' => $prefix . 'percent_txt',
        'type' => 'text'
    ),
    array(
        'label' => __('Page Background Image URL', 'sati-wp'),
        'desc' => '',
        'id' => $prefix . 'background_img',
        'type' => 'text'
    ),
    array(
        'label' => __('Background Image Position', 'sati-wp'),
        'desc' => '',
        'id' => $prefix . 'img_position',
        'type' => 'select',
        'options' => array(
            'one' => array(
                'label' => 'Left Top',
                'value' => 'left top'
            ),
            'two' => array(
                'label' => 'Left Center',
                'value' => 'left center'
            ),
            'three' => array(
                'label' => 'Left Bottom',
                'value' => 'left bottom'
            ),
            'four' => array(
                'label' => 'Center Top',
                'value' => 'center top'
            ),
            'five' => array(
                'label' => 'Center Center',
                'value' => 'center center'
            ),
            'six' => array(
                'label' => 'Center Bottom',
                'value' => 'center bottom'
            ),
            'seven' => array(
                'label' => 'Right Top',
                'value' => 'right top'
            ),
            'eight' => array(
                'label' => 'Right Center',
                'value' => 'right center'
            ),
            'nine' => array(
                'label' => 'Right Bottom',
                'value' => 'right bottom'
            )
        )
    ),
    array(
        'label' => __('Background Image Repeat', 'sati-wp'),
        'desc' => '',
        'id' => $prefix . 'img_repeat',
        'type' => 'select',
        'options' => array(
            'one' => array(
                'label' => 'No Repeat',
                'value' => 'no-repeat'
            ),
            'two' => array(
                'label' => 'Repeat-X',
                'value' => 'repeat-x'
            ),
            'three' => array(
                'label' => 'Repeat-Y',
                'value' => 'repeat-y'
            ),
            'four' => array(
                'label' => 'Repeat All',
                'value' => 'repeat'
            )
        )
    ), array(
        'label' => __('Page Structure', 'sati-wp'),
        'desc' => '',
        'id' => $prefix . 'structure',
        'type' => 'select',
        'options' => array(
            'one' => array(
                'label' => __('Include in One Page', 'sati-wp'),
                'value' => '1'
            ),
            'two' => array(
                'label' => __('Separated / Stand Alone Page', 'sati-wp'),
                'value' => '2'
            )
        )
    )
);

// The Callback  
function sati_show_page_custom_meta_box() {
    global $page_custom_meta_fields, $post;
// Use nonce for verification  
    echo '<input type="hidden" name="custom_meta_box_nonce" value="' . wp_create_nonce(basename(__FILE__)) . '" />';
// Begin the field table and loop  
    echo '<table class="form-table">';
    foreach ($page_custom_meta_fields as $field) {
// get value of this field if it exists for this post  
        $meta = get_post_meta($post->ID, $field['id'], true);
// begin a table row with  
        echo '<tr> 
                <th><label for="' . $field['id'] . '">' . $field['label'] . '</label></th> 
                <td>';
        switch ($field['type']) {
// case items will go here  
// checkbox  
            case 'checkbox':
                echo '<input type="checkbox" name="' . $field['id'] . '" id="' . $field['id'] . '" ', $meta ? ' checked="checked"' : '', '/> 
        <label for="' . $field['id'] . '">' . $field['desc'] . '</label>';
                break;
// text  
            case 'text':
                if ($field['id'] == 'page_background_img') {
                    echo '<label for="upload_image">
				<input id="' . $field['id'] . '" type="text" size="36" name="' . $field['id'] . '" value="' . $meta . '" /> 
				<input id="upload_image_button" class="button" type="button" value="Upload Image" />
				<br />Enter a URL or upload an image
				</label>';
                } else {
                    echo '<input type="text" name="' . $field['id'] . '" id="' . $field['id'] . '" value="' . $meta . '" size="50" /> 
        <br /><span class="description">' . $field['desc'] . '</span>';
                }
                break;
// select  
            case 'select':
                echo '<select name="' . $field['id'] . '" id="' . $field['id'] . '">';
                foreach ($field['options'] as $option) {
                    echo '<option', $meta == $option['value'] ? ' selected="selected"' : '', ' value="' . $option['value'] . '">' . $option['label'] . '</option>';
                }
                echo '</select><br /><span class="description">' . $field['desc'] . '</span>';
                break;
// textarea  
            case 'textarea':
                echo '<textarea name="' . $field['id'] . '" id="' . $field['id'] . '" cols="60" rows="4">' . $meta . '</textarea> 
					<br /><span class="description">' . $field['desc'] . '</span>';
                break;
        } //end switch  
        echo '</td></tr>';
    } // end foreach  
    echo '</table>'; // end table  
}

// Save the Data  
function sati_save_page_custom_meta($post_id) {
    global $page_custom_meta_fields;
// verify nonce  
    if (isset($_POST['custom_meta_box_nonce'])) {
        if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__))) {
            return $post_id;
        }
    }
// check autosave  
// Stop WP from clearing custom fields on autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return;
// Prevent quick edit from clearing custom fields
    if (defined('DOING_AJAX') && DOING_AJAX)
        return;
// check permissions  
    if (isset($_POST['post_type']) && 'page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id))
            return $post_id;
    } elseif (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }
// loop through fields and save the data  
    foreach ($page_custom_meta_fields as $field) {
        $old = get_post_meta($post_id, $field['id'], true);
        $new = null;
        if (isset($_POST[$field['id']])) {
            $new = $_POST[$field['id']];
        }
        if ($new && $new != $old) {
            update_post_meta($post_id, $field['id'], $new);
        } elseif ('' == $new && $old) {
            delete_post_meta($post_id, $field['id'], $old);
        }
    } // end foreach  
}

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Custom error handler">
function sati_get_dry_die_handler() {
    return 'sati_dry_die_handler';
}

function sati_dry_die_handler($message, $title = '', $args = array()) {
    $errorTemplate = get_theme_root() . '/' . get_template() . '/error.php';
    if (!is_admin() && file_exists($errorTemplate)) {
        $defaults = array('response' => 500);
        $r = wp_parse_args($args, $defaults);
        $have_gettext = function_exists('__');
        if (function_exists('is_wp_error') && is_wp_error($message)) {
            if (empty($title)) {
                $error_data = $message->get_error_data();
                if (is_array($error_data) && isset($error_data['title']))
                    $title = $error_data['title'];
            }
            $errors = $message->get_error_messages();
            switch (count($errors)) :
                case 0 :
                    $message = '';
                    break;
                case 1 :
                    $message = "<p>{$errors[0]}</p>";
                    break;
                default :
                    $message = "<ul>\n\t\t<li>" . join("</li>\n\t\t<li>", $errors) . "</li>\n\t</ul>";
                    break;
            endswitch;
        } elseif (is_string($message)) {
            $message = "<p>$message</p>";
        }
        if (isset($r['back_link']) && $r['back_link']) {
            $back_text = $have_gettext ? '&laquo; Back' : '&laquo; Back';
            $message .= "\n<p><a href='javascript:history.back()'>$back_text</a></p>";
        }
        if (empty($title))
            $title = $have_gettext ? 'WordPress &rsaquo; Error' : 'WordPress &rsaquo; Error';
        require_once($errorTemplate);
        die();
    } else {
        _default_wp_die_handler($message, $title, $args);
    }
}

//</editor-fold>
// <editor-fold defaultstate="collapsed" desc="Register theme menu">
function sati_register_menu() {
    register_nav_menu('custom_menu', 'Main Menu');
}

// </editor-fold>
//<editor-fold defaultstate="collapsed" desc="Columns short code">
function sati_col($atts, $content = null) {
    extract(shortcode_atts(array(
        "size" => 'one',
        "class" => ''
                    ), $atts));

    switch ($size) {
        case "one":
            $return = '<div class = "one ' . $class . '">
    ' . do_shortcode($content) . '
    </div><div class = "clear"></div>';
            break;
        case "one_half_last":
            $return = '<div class = "one_half last ' . $class . '">' . do_shortcode($content) . '</div><div class = "clear"></div>';
            break;
        case "one_third_last":
            $return = '<div class = "one_third last ' . $class . '">' . do_shortcode($content) . '</div><div class = "clear"></div>';
            break;
        case "two_third_last":
            $return = '<div class = "two_third last ' . $class . '">' . do_shortcode($content) . '</div><div class = "clear"></div>';
            break;
        case "one_fourth_last":
            $return = '<div class = "one_fourth last ' . $class . '">' . do_shortcode($content) . '</div><div class = "clear"></div>';
            break;
        case "three_fourth_last":
            $return = '<div class = "three_fourth last ' . $class . '">' . do_shortcode($content) . '</div><div class = "clear"></div>';
            break;
        default:
            $return = '<div class = "' . $size . ' ' . $class . '">' . do_shortcode($content) . '</div>';
    }

    return $return;
}

add_shortcode("col", "sati_col");

// </editor-fold>
//<editor-fold defaultstate="collapsed" desc="BR short code">
function sati_br($atts, $content = null) {
    return '<br />';
}

add_shortcode("br", "sati_br");

//</editor-fold>
// <editor-fold defaultstate="collapsed" desc="Skills short code">
function sati_skills($atts, $content = null) {
    extract(shortcode_atts(array(
        "class" => '',
        "title" => '',
        "percent" => '50%'
                    ), $atts));

    $return = '<div class="progress_bar ' . $class . '">                         
               <div class="progress_bar_field_holder" style="width:' . $percent . ';">  
               <div class="progress_bar_title" style="color: ' . $color . '">' . $title . '</div>    
               <div class="progress_bar_percent_text">' . $percent . '</div>                       
               <div class="progress_bar_field_perecent"></div>    
               </div>              
               </div>';
    return $return;
}

add_shortcode("skills", "sati_skills");

// </editor-fold>
//<editor-fold defaultstate="collapsed" desc="Animated Header Text short code">
function sati_atxt($atts, $content = null) {
    extract(shortcode_atts(array(
        "class" => '',
        "words" => '',
        "endtxt" => '',
                    ), $atts));

    $return = '<span class = "entry-title cd-headline letters type ' . $class . '">
                <span>' . $content . '</span>
                <span class = "cd-words-wrapper waiting">';
    $c = 0;
    $words = explode(',', $words);

    foreach ($words as $key => $value) {
        if ($c < 1) {
            $return .= '<b class = "is-visible">' . $value . '</b>';
        } else {
            $return .= '<b>' . $value . '</b>';
        }
        $c++;
    }

    if ($endtxt != '') {
        $return .= '</span><span class="end-txt">' . $endtxt . '</span></span>';
    } else {
        $return .= '</span></span>';
    }

    return $return;
}

add_shortcode("atxt", "sati_atxt");

//</editor-fold>
// <editor-fold defaultstate="collapsed" desc="Button short code">
function sati_button($atts, $content = null) {
    extract(shortcode_atts(array(
        "class" => '',
        "target" => '_self',
        "href" => '#',
        "position" => 'left'
                    ), $atts));

    switch ($position) {
        case 'center':
            $position = "center-text";
            break;
        case 'right':
            $position = "text-right";
            break;
        default:
            $position = "text-left";
    }

    $return = '<div class="' . $position . '"><a href="' . $href . '" target="' . $target . '" class="button scroll ' . $class . '">' . do_shortcode($content) . '</a></div>';

    return $return;
}

add_shortcode("button", "sati_button");

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Unregister the Sidebar">
function sati_widgets_init() {
    unregister_sidebar('sidebar');
}

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Custom menu Walker">
class Sati_Header_Menu extends Walker_Nav_Menu {

    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {

        global $post;
        global $wp_query;

        $indent = ( $depth ) ? str_repeat("\t", $depth) : '';

        $class_names = $value = '';
        $classes = empty($item->classes) ? array() : (array) $item->classes;
        $class_names = join(' ', $classes);

        $class_names = ' class = "' . esc_attr($class_names) . '"';


        $attributes = !empty($item->target) ? ' target = "' . esc_attr($item->target) . '"' : '';
        $attributes .=!empty($item->xfn) ? ' rel = "' . esc_attr($item->xfn) . '"' : '';


        if ($item->object == 'page') {
            $post_object = get_post($item->object_id);

            $output .= $indent . '<li id = "menu-item-' . $item->ID . '"' . $value . $class_names . '>';

            $page_structure = get_post_meta($item->object_id, "page_structure", true);

            if ('frontpage.php' == get_page_template_slug($post->ID)) {
                if ($page_structure == 1) {
                    $attributes .= ' href = "#' . $post_object->post_name . '" class = "scroll"';
                } else {
                    $attributes .=!empty($item->url) ? ' href = "' . esc_attr($item->url) . '" class = "no-scroll"' : '';
                }
            } else {

                if ($page_structure == 1) {
                    $attributes .= ' href = "' . home_url() . '/#' . $post_object->post_name . '"';
                } else {
                    $attributes .=!empty($item->url) ? ' href = "' . esc_attr($item->url) . '" class = "no-scroll"' : '';
                }
            }



            $item_output = $args->before;
            $item_output .= '<a' . $attributes . '>';
            $item_output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID);
            $item_output .= $args->link_after;
            $item_output .= '</a>';
            $item_output .= $args->after;

            $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
        } else {

            $output .= $indent . '<li id = "menu-item-' . $item->ID . '"' . $value . $class_names . '>';

            $attributes .=!empty($item->url) ? ' href = "' . esc_attr($item->url) . '" class = "custom-link"' : '';

            $item_output = $args->before;
            $item_output .= '<a' . $attributes . '>';
            $item_output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID);
            $item_output .= $args->link_after;
            $item_output .= '</a>';
            $item_output .= $args->after;

            $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
        }
    }

}

//</editor-fold>
// <editor-fold defaultstate="collapsed" desc="Custom WP_TITLE function for older versions">
if (!function_exists('_wp_render_title_tag')) {

    function theme_slug_render_title() {
        ?>
        <title><?php wp_title('|', true, 'right'); ?></title>
        <?php
    }

    add_action('wp_head', 'theme_slug_render_title');
}
//</editor-fold>
// <editor-fold defaultstate="collapsed" desc="TGM Plugin">
//-----------******************----------------//

/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * It is expected that theme authors would copy and paste this code into their
 * functions.php file, and amend to suit.
 *
 * @see http://tgmpluginactivation.com/configuration/ for detailed documentation.
 *
 * @package    TGM-Plugin-Activation
 * @subpackage Example
 * @version    2.5.2
 * @author     Thomas Griffin, Gary Jones, Juliette Reinders Folmer
 * @copyright  Copyright (c) 2011, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/TGMPA/TGM-Plugin-Activation
 */

/**
 * Register the required plugins for this theme.
 *
 * In this example, we register five plugins:
 * - one included with the TGMPA library
 * - two from an external source, one from an arbitrary source, one from a GitHub repository
 * - two from the .org repo, where one demonstrates the use of the `is_callable` argument
 *
 * The variable passed to tgmpa_register_plugins() should be an array of plugin
 * arrays.
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * TGM_Plugin_Activation class constructor.
 */
function my_theme_register_required_plugins() {
    /*
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(
        // This is an example of how to include a plugin bundled with a theme.
        array(
            'name' => 'Dry Shortcode', // The plugin name
            'slug' => 'dry-shortcode', // The plugin slug (typically the folder name)
            'source' => get_stylesheet_directory() . '/lib/plugins/dry-shortcode.zip', // The plugin source
            'required' => false, // If false, the plugin is only 'recommended' instead of required
            'version' => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url' => '', // If set, overrides default API URL and points to an external URL
        ),
        array(
            'name' => 'Dry Preview', // The plugin name
            'slug' => 'dry-preview', // The plugin slug (typically the folder name)
            'source' => get_stylesheet_directory() . '/lib/plugins/dry-preview.zip', // The plugin source
            'required' => false, // If false, the plugin is only 'recommended' instead of required
            'version' => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url' => '', // If set, overrides default API URL and points to an external URL
        ),
        array(
            'name' => 'Contact Form 7', // The plugin name
            'slug' => 'contact-form-7', // The plugin slug (typically the folder name)
            'source' => '', // The plugin source
            'required' => false, // If false, the plugin is only 'recommended' instead of required
            'version' => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url' => 'https://wordpress.org/plugins/contact-form-7/', // If set, overrides default API URL and points to an external URL
        )
    );

    /*
     * Array of configuration settings. Amend each line as needed.
     *
     * TGMPA will start providing localized text strings soon. If you already have translations of our standard
     * strings available, please help us make TGMPA even better by giving us access to these translations or by
     * sending in a pull-request with .po file(s) with the translations.
     *
     * Only uncomment the strings in the config array if you want to customize the strings.
     */
    $config = array(
        'id' => 'tgmpa', // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '', // Default absolute path to bundled plugins.
        'menu' => 'tgmpa-install-plugins', // Menu slug.
        'parent_slug' => 'themes.php', // Parent menu slug.
        'capability' => 'edit_theme_options', // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
        'has_notices' => true, // Show admin notices or not.
        'dismissable' => true, // If false, a user cannot dismiss the nag message.
        'dismiss_msg' => '', // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false, // Automatically activate plugins after installation or not.
        'message' => '', // Message to output right before the plugins table.
        'strings' => array(
            'page_title' => __('Install Required Plugins', 'sati-wp'),
            'menu_title' => __('Install Plugins', 'sati-wp'),
            'installing' => __('Installing Plugin: %s', 'sati-wp'), // %s = plugin name.
            'oops' => __('Something went wrong with the plugin API.', 'sati-wp'),
            'notice_can_install_required' => _n_noop(
                    'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'sati-wp'
            ), // %1$s = plugin name(s).
            'notice_can_install_recommended' => _n_noop(
                    'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'sati-wp'
            ), // %1$s = plugin name(s).
            'notice_cannot_install' => _n_noop(
                    'Sorry, but you do not have the correct permissions to install the %1$s plugin.', 'Sorry, but you do not have the correct permissions to install the %1$s plugins.', 'sati-wp'
            ), // %1$s = plugin name(s).
            'notice_ask_to_update' => _n_noop(
                    'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'sati-wp'
            ), // %1$s = plugin name(s).
            'notice_ask_to_update_maybe' => _n_noop(
                    'There is an update available for: %1$s.', 'There are updates available for the following plugins: %1$s.', 'sati-wp'
            ), // %1$s = plugin name(s).
            'notice_cannot_update' => _n_noop(
                    'Sorry, but you do not have the correct permissions to update the %1$s plugin.', 'Sorry, but you do not have the correct permissions to update the %1$s plugins.', 'sati-wp'
            ), // %1$s = plugin name(s).
            'notice_can_activate_required' => _n_noop(
                    'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'sati-wp'
            ), // %1$s = plugin name(s).
            'notice_can_activate_recommended' => _n_noop(
                    'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'sati-wp'
            ), // %1$s = plugin name(s).
            'notice_cannot_activate' => _n_noop(
                    'Sorry, but you do not have the correct permissions to activate the %1$s plugin.', 'Sorry, but you do not have the correct permissions to activate the %1$s plugins.', 'sati-wp'
            ), // %1$s = plugin name(s).
            'install_link' => _n_noop(
                    'Begin installing plugin', 'Begin installing plugins', 'sati-wp'
            ),
            'update_link' => _n_noop(
                    'Begin updating plugin', 'Begin updating plugins', 'sati-wp'
            ),
            'activate_link' => _n_noop(
                    'Begin activating plugin', 'Begin activating plugins', 'sati-wp'
            ),
            'return' => __('Return to Required Plugins Installer', 'sati-wp'),
            'plugin_activated' => __('Plugin activated successfully.', 'sati-wp'),
            'activated_successfully' => __('The following plugin was activated successfully:', 'sati-wp'),
            'plugin_already_active' => __('No action taken. Plugin %1$s was already active.', 'sati-wp'), // %1$s = plugin name(s).
            'plugin_needs_higher_version' => __('Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'sati-wp'), // %1$s = plugin name(s).
            'complete' => __('All plugins installed and activated successfully. %1$s', 'sati-wp'), // %s = dashboard link.
            'contact_admin' => __('Please contact the administrator of this site for help.', 'sati-wp'),
            'nag_type' => 'updated', // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
        ),
    );

    tgmpa($plugins, $config);
}

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Archive title filter">
function my_theme_archive_title($title) {
    if (is_category()) {
        $title = single_cat_title('', false);
    } elseif (is_tag()) {
        $title = single_tag_title('', false);
    } elseif (is_author()) {
        $title = get_the_author();
    } elseif (is_post_type_archive()) {
        $title = post_type_archive_title('', false);
    } elseif (is_tax()) {
        $title = single_term_title('', false);
    }

    return $title;
}

//</editor-fold>
// <editor-fold defaultstate="collapsed" desc="Shortcodes p-tag fix">
function the_content_filter($content) {
// array of custom shortcodes requiring the fix 
    $block = join("|", array("col", "atxt", "portfolio_holder", "portfolio", "skills"));
// opening tag
    $rep = preg_replace("/(<p>)?\[($block)(\s[^\]]+)?\](<\/p>|<br \/>)?/", "[$2$3]", $content);

// closing tag
    $rep = preg_replace("/(<p>)?\[\/($block)](<\/p>|<br \/>)?/", "[/$2]", $rep);
    return $rep;
}

//</editor-fold>
?>
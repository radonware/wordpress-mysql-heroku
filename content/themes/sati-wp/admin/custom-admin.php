<?php
/*
 * Register Theme Customizer
 */
add_action('customize_register', 'drytheme_customize_register');

function drytheme_customize_register($wp_customize) {

    class Example_Customize_Textarea_Control extends WP_Customize_Control {

        public $type = 'textarea';

        public function render_content() {
            ?>
            <label>
                <span class="customize-control-title"><?php echo esc_html($this->label); ?></span>
                <textarea rows="5" style="width:100%;" <?php $this->link(); ?>><?php echo esc_textarea($this->value()); ?></textarea>
            </label>
            <?php
        }

    }

    //----------------------------------COLORS SECTION--------------------

    function sati_get_color_scheme_choices() {
        $color_schemes = sati_get_color_schemes();
        $color_scheme_control_options = array();

        foreach ($color_schemes as $color_scheme => $value) {
            $color_scheme_control_options[$color_scheme] = $value['label'];
        }

        return $color_scheme_control_options;
    }

    function sati_get_color_schemes() {
        return apply_filters('sati_color_schemes', array(
            'light' => array(
                'label' => __('Light', 'sati-wp'),
                'colors' => array(
                    '#2db48b',
                    '#494949',
                    '#ffffff',
                    '#a2a2a2',
                    '#28a77f'
                ),
            ),
            'dark' => array(
                'label' => __('Dark', 'sati-wp'),
                'colors' => array(
                    '#1aadd7',
                    '#D7DADF',
                    '#2B2C30',
                    '#25262A',
                    '#0099b5'
                ),
            )
        ));
    }

    $wp_customize->add_setting('color_scheme', array(
        'default' => 'light',
        'sanitize_callback' => 'balanceTags',
        'transport' => 'postMessage',
    ));

    $wp_customize->add_control('color_scheme', array(
        'label' => __('Base Color Scheme', 'sati-wp'),
        'section' => 'colors',
        'type' => 'select',
        'choices' => sati_get_color_scheme_choices(),
        'priority' => 1,
    ));



    $wp_customize->add_setting('global_color', array(
        'default' => '#2db48b',
        'type' => 'option',
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_hex_color'
    ));
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'global_color', array(
        'label' => __('Global Color', 'sati-wp'),
        'section' => 'colors',
        'settings' => 'global_color'
    )));




    $wp_customize->add_setting('body_color', array(
        'default' => '#494949',
        'type' => 'option',
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_hex_color'
    ));
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'body_color', array(
        'label' => __('Body Color', 'sati-wp'),
        'section' => 'colors',
        'settings' => 'body_color'
    )));



    $wp_customize->add_setting('body_background', array(
        'default' => '#ffffff',
        'type' => 'option',
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_hex_color'
    ));
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'body_background', array(
        'label' => __('Body Background', 'sati-wp'),
        'section' => 'colors',
        'settings' => 'body_background'
    )));


    $wp_customize->add_setting('subscribe_background', array(
        'default' => '#a2a2a2',
        'type' => 'option',
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_hex_color'
    ));
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'subscribe_background', array(
        'label' => __('Subscribe Email Background', 'sati-wp'),
        'section' => 'colors',
        'settings' => 'subscribe_background'
    )));
        
 
    $wp_customize->add_setting('subscribe_submit_hover_background', array(
        'default' => '#28a77f',
        'type' => 'option',
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_hex_color'
    ));
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'subscribe_submit_hover_background', array(
        'label' => __('Subscribe Hover Background', 'sati-wp'),
        'section' => 'colors',
        'settings' => 'subscribe_submit_hover_background'
    )));


    //----------------------------- END COLORS SECTION  ---------------------------------------------
    //    
    //
    //
    //
    //----------------------------- IMAGE SECTION  ---------------------------------------------

    $wp_customize->add_section('dry_image_section', array(
        'title' => __('Images Section', 'sati-wp'),
        'priority' => 37
    ));

    $wp_customize->add_setting('sati_preloader', array(
        'default' => get_template_directory_uri() . '/images/ajax-document-loader.gif',
        'capability' => 'edit_theme_options',
        'type' => 'option',
        'sanitize_callback' => 'sanitize_text_field'
    ));
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'sati_preloader', array(
        'label' => __('Preloader Gif', 'sati-wp'),
        'section' => 'dry_image_section',
        'settings' => 'sati_preloader'
    )));


    $wp_customize->add_setting('sati_header_logo', array(
        'default' => get_template_directory_uri() . '/images/satiDefaultLogo.png',
        'capability' => 'edit_theme_options',
        'type' => 'option',
        'sanitize_callback' => 'sanitize_text_field'
    ));
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'sati_header_logo', array(
        'label' => __('Header Logo', 'sati-wp'),
        'section' => 'dry_image_section',
        'settings' => 'sati_header_logo'
    )));


    //----------------------------- END IMAGE SECTION  ---------------------------------------------
    //--------------------------------------------------------------------------
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->get_setting('blogdescription')->transport = 'postMessage';
    $wp_customize->get_setting('global_color')->transport = 'postMessage';
    $wp_customize->get_setting('body_color')->transport = 'postMessage';
    $wp_customize->get_setting('body_background')->transport = 'postMessage';
    $wp_customize->get_setting('subscribe_background')->transport = 'postMessage';
    $wp_customize->get_setting('subscribe_submit_hover_background')->transport = 'postMessage';

    //--------------------------------------------------------------------------
    /*
     * If preview mode is active, hook JavaScript to preview changes
     */
    if ($wp_customize->is_preview() && !is_admin()) {
        add_action('customize_preview_init', 'drytheme_customize_preview_js');
    }
}

/**
 * Bind Theme Customizer JavaScript
 */
function drytheme_customize_preview_js() {
    wp_enqueue_script('drytheme-customizer', get_template_directory_uri() . '/admin/js/custom-admin.js', array('customize-preview'), '20120910', true);
}

/*
 * Generate CSS Styles
 */
add_action('wp_head', 'drytheme_customized_style');

function drytheme_customized_style() {
    echo ('<style type="text/css">');
    dry_generate_css('a.button, body a:hover, .sm-clean .current_page_item a, .main-menu.sm-clean a:hover, p.read-more a, .comment-reply-link, .replay-at-author, .form-submit #submit, .page-numbers.current, .page-numbers:hover, .wpcf7 input[type=submit], span.wpcf7-not-valid-tip', 'color', 'global_color');
    dry_generate_css('a.button, .form-submit #submit, .page-numbers.current, .wpcf7 input[type=submit]', 'border-color', 'global_color');
    dry_generate_css('a.button:hover, .toggle-holder:hover #toggle div, .main-menu.sm-clean a:after, .cd-headline.type .cd-words-wrapper.selected, .right-mask, .progress_bar_field_perecent, p.read-more a:after, .form-submit #submit:hover, .wpcf7 input[type=submit]:hover', 'background-color', 'global_color');
   
    dry_generate_css('::selection', 'background-color', 'global_color');
    dry_generate_css('::-moz-selection', 'background-color', 'global_color');
    
    dry_generate_css('body, body a', 'color', 'body_color');
    dry_generate_css('body, .doc-loader', 'background-color', 'body_background');
    dry_generate_css('.subscribe-form .wpcf7 input[type=email], .left-mask', 'background-color', 'subscribe_background');
    dry_generate_css('.right-mask.hover', 'background-color', 'subscribe_submit_hover_background');
    
    echo ('</style>');
}

/*
 * Generate CSS Class - Helper Method
 */

function dry_generate_css($selector, $style, $mod_name, $prefix = '', $postfix = '', $echo = true) {
    $return = '';
    $mod = get_option($mod_name);
    if (!empty($mod)) {
        $return = sprintf('%s { %s:%s; }', $selector, $style, $prefix . $mod . $postfix
        );
        if ($echo) {
            echo $return;
        }
    }
    return $return;
}
?>
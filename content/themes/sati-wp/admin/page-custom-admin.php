<?php

function rea_options_admin_styles() {
    wp_register_style('wp-custom-dry_admin_layout_css', get_template_directory_uri('template_url') . '/admin/css/layout.css', false, '1.0.0');
    wp_enqueue_style('wp-custom-dry_admin_layout_css');
}

function sati_my_admin_scripts() {
    wp_enqueue_media();
    wp_register_script('my-admin-js', get_template_directory_uri() . '/admin/js/admin.js', array('jquery'));
    wp_enqueue_script('my-admin-js');
}

function sati_customize_control_js() {
    wp_enqueue_script('color-scheme-control', get_template_directory_uri() . '/admin/js/color-scheme-control.js', array('customize-controls', 'iris', 'underscore', 'wp-util'), '20141216', true);
    wp_localize_script('color-scheme-control', 'colorScheme', sati_get_color_schemes());
}

if (is_admin()) {
    add_action('admin_print_styles', 'rea_options_admin_styles');
    add_action('admin_enqueue_scripts', 'sati_my_admin_scripts');
    add_action('customize_controls_enqueue_scripts', 'sati_customize_control_js');
}
?>
(function (api) {
    var cssTemplate = wp.template('sati-color-scheme'),
            colorSchemeKeys = [
                'global_color',
                'body_color',
                'body_background',
                'subscribe_background',
                'subscribe_submit_hover_background'
            ],
            colorSettings = [
                'global_color',
                'body_color',
                'body_background',
                'subscribe_background',
                'subscribe_submit_hover_background'
            ];

    api.controlConstructor.select = api.Control.extend({
        ready: function () {
            if ('color_scheme' === this.id) {
                this.setting.bind('change', function (value) {
                    // Update Background Color.
                    api('global_color').set(colorScheme[value].colors[0]);
                    api.control('global_color').container.find('.color-picker-hex')
                            .data('data-default-color', colorScheme[value].colors[0])
                            .wpColorPicker('defaultColor', colorScheme[value].colors[0]);

                    // Update Background Color.
                    api('body_color').set(colorScheme[value].colors[1]);
                    api.control('body_color').container.find('.color-picker-hex')
                            .data('data-default-color', colorScheme[value].colors[1])
                            .wpColorPicker('defaultColor', colorScheme[value].colors[1]);

                    // Update Background Color.
                    api('body_background').set(colorScheme[value].colors[2]);
                    api.control('body_background').container.find('.color-picker-hex')
                            .data('data-default-color', colorScheme[value].colors[2])
                            .wpColorPicker('defaultColor', colorScheme[value].colors[2]);
                    
                    // Update Background Color.
                    api('subscribe_background').set(colorScheme[value].colors[3]);
                    api.control('subscribe_background').container.find('.color-picker-hex')
                            .data('data-default-color', colorScheme[value].colors[3])
                            .wpColorPicker('defaultColor', colorScheme[value].colors[3]);
                    
                    // Update Background Color.
                    api('subscribe_submit_hover_background').set(colorScheme[value].colors[4]);
                    api.control('subscribe_submit_hover_background').container.find('.color-picker-hex')
                            .data('data-default-color', colorScheme[value].colors[4])
                            .wpColorPicker('defaultColor', colorScheme[value].colors[4]);

                });
            }
        }
    });

})(wp.customize);
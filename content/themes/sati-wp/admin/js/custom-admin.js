(function ($) {


    // COLORS                         
    wp.customize('global_color', function (value) {
        value.bind(function (to) {
            var inlineStyle, customColorCssElemnt;
            inlineStyle = '<style class="custom-color-css1">';

            inlineStyle += 'a.button, body a:hover, .sm-clean .current_page_item a, .main-menu.sm-clean a:hover, p.read-more a, .comment-reply-link, .replay-at-author, .form-submit #submit, .page-numbers.current, .page-numbers:hover, .wpcf7 input[type=submit], span.wpcf7-not-valid-tip { color: ' + to + '; }';
            inlineStyle += 'a.button, .form-submit #submit, .page-numbers.current, .wpcf7 input[type=submit] { border-color: ' + to + '; }';
            inlineStyle += 'a.button:hover, .toggle-holder:hover #toggle div, .main-menu.sm-clean a:after, .cd-headline.type .cd-words-wrapper.selected, .right-mask, .progress_bar_field_perecent, p.read-more a:after, .form-submit #submit:hover, .wpcf7 input[type=submit]:hover { background-color: ' + to + '; }';
            inlineStyle += '::selection { background-color: ' + to + '; }';
            inlineStyle += '-moz-selection { background-color: ' + to + '; }';

            inlineStyle += '</style>';

            customColorCssElemnt = $('.custom-color-css1');

            if (customColorCssElemnt.length) {
                customColorCssElemnt.replaceWith(inlineStyle);
            } else {
                $('head').append(inlineStyle);
            }

        });
    });


    wp.customize('body_color', function (value) {
        value.bind(function (to) {
            var inlineStyle, customColorCssElemnt;
            inlineStyle = '<style class="custom-color-css2">';

            inlineStyle += 'body, body a { color: ' + to + '; }';

            inlineStyle += '</style>';

            customColorCssElemnt = $('.custom-color-css2');

            if (customColorCssElemnt.length) {
                customColorCssElemnt.replaceWith(inlineStyle);
            } else {
                $('head').append(inlineStyle);
            }

        });
    });


    wp.customize('body_background', function (value) {
        value.bind(function (to) {
            var inlineStyle, customColorCssElemnt;
            inlineStyle = '<style class="custom-color-css3">';

            inlineStyle += 'body, .doc-loader { background-color: ' + to + '; }';

            inlineStyle += '</style>';

            customColorCssElemnt = $('.custom-color-css3');

            if (customColorCssElemnt.length) {
                customColorCssElemnt.replaceWith(inlineStyle);
            } else {
                $('head').append(inlineStyle);
            }

        });
    });


    wp.customize('subscribe_background', function (value) {
        value.bind(function (to) {
            var inlineStyle, customColorCssElemnt;
            inlineStyle = '<style class="custom-color-css4">';

            inlineStyle += '.subscribe-form .wpcf7 input[type=email], .left-mask { background-color: ' + to + '; }';

            inlineStyle += '</style>';

            customColorCssElemnt = $('.custom-color-css4');

            if (customColorCssElemnt.length) {
                customColorCssElemnt.replaceWith(inlineStyle);
            } else {
                $('head').append(inlineStyle);
            }

        });
    });
    
    
    wp.customize('subscribe_submit_hover_background', function (value) {
        value.bind(function (to) {
            var inlineStyle, customColorCssElemnt;
            inlineStyle = '<style class="custom-color-css5">';

            inlineStyle += '.right-mask.hover { background-color: ' + to + '; }';

            inlineStyle += '</style>';

            customColorCssElemnt = $('.custom-color-css5');

            if (customColorCssElemnt.length) {
                customColorCssElemnt.replaceWith(inlineStyle);
            } else {
                $('head').append(inlineStyle);
            }

        });
    });



})(jQuery);
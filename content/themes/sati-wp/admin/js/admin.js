jQuery(document).ready(function (jQuery) {


    var custom_uploader;


    jQuery('#upload_image_button').click(function (e) {

        var return_field = jQuery(this).prev();

        e.preventDefault();


        //If the uploader object has already been created, reopen the dialog
        if (custom_uploader) {
            custom_uploader.open();
            return;
        }

        //Extend the wp.media object
        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Choose Image',
            button: {
                text: 'Choose Image'
            },
            multiple: false
        });

        //When a file is selected, grab the URL and set it as the text field's value
        custom_uploader.on('select', function () {
            attachment = custom_uploader.state().get('selection').first().toJSON();
            jQuery(return_field).val(attachment.url);
        });

        //Open the uploader dialog
        custom_uploader.open();

    });


    if (jQuery('#page_template').val() === 'frontpage.php')
    {
        jQuery('#page_custom_meta_box .form-table tr').show();
    } else
    {
        jQuery('#page_custom_meta_box .form-table tr').last().show();
    }


    jQuery('#page_template').on('change', function () {
        if (this.value === 'frontpage.php')
        {
            jQuery('#page_custom_meta_box .form-table tr').show();

        } else
        {
            jQuery('#page_custom_meta_box .form-table tr').not(':last').hide();
        }
    });


});
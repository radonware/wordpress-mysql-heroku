<?php get_header(); ?>

<header class = "header-holder center-relative relative">
    <div class = "menu-wraper center-relative">
        <?php
        if (has_nav_menu("custom_menu")) {
            wp_nav_menu(
                    array(
                        "container" => "nav",
                        "container_class" => "big-menu",
                        "container_id" => "header-main-menu",
                        "fallback_cb" => false,
                        "menu_class" => "main-menu sm sm-clean",
                        "theme_location" => "custom_menu",
                        "items_wrap" => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        "walker" => new Sati_Header_Menu()
                    )
            );
        } else {
            echo '<nav id="header-main-menu" class="big-menu">';
            wp_page_menu(array('menu_class' => 'default-menu', 'depth' => '3'));
            echo '</nav>';
        }
        ?>
        <div class="clear"></div>
    </div>                       
</header>

<?php
$return = "";
if (have_posts()) :
    $return .= ' <div class="block content-1170 center-relative entry-content blog-holder">';
    while (have_posts()) : the_post();


        $return .= '<article id="post-' . $post->ID . '"  class="relative blog-item-holder center-relative">';
        $return .= '<h2 class="entry-title"><a href="' . get_permalink($post->ID) . '">' . get_the_title() . '</a></h2>
                    <div class="entry-content">' . the_excerpt_max_charlength(140) . '</div>
                    <div class="clear"></div>';
        $return .= '</article>';

    endwhile;
    $return .= '</div>';
endif;
?>

<h1 class="entry-title">"<?php echo get_the_archive_title(); ?>"</h1>	 

<?php
echo $return;

$big = 99999;
echo '<div class="page-pagination-holder center-text">';
echo paginate_links(array(
    'base' => str_replace($big, '%#%', html_entity_decode(get_pagenum_link($big))),
    'format' => '?paged=%#%',
    'current' => max(1, get_query_var('paged')),
    'total' => $wp_query->max_num_pages,
    'prev_next' => false
));
echo '</div>';
?>   
<?php get_footer(); ?>